
<?php
include 'header.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "select idautos, modelo, a.precio, a.placa, a.descripcion, a.anio , a.idmarca, m.descripcion marca, a.idcolor ,c.descripcion color
from autos a 
inner join marca m on m.idmarca = a.idmarca
inner join color c on c.idcolor = a.idcolor
where a.idestado = 1 and idautos = $id
order by marca;";
    //echo $sql;
    $result = mysql_query($sql, $conexion);
    while ($row = mysql_fetch_array($result)) {
        $modelo = $row['modelo'];
        $precio = $row['precio'];
        $placa = $row['placa'];
        $anio = $row['anio'];
        $descripcion = $row['descripcion'];
        $marca = $row['marca'];
        $idmarca = $row['idmarca'];
        $color = $row['color'];
        $idcolor = $row['idcolor'];
    }
    ?>

    <form class="contact_form" action="../../sisWeb/model/modificar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Modificar los datos de <?php echo $nombre; ?></h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <li>
                <label for="idmarca">Marca:</label>
                <select id="idmarca" name="idmarca" title="Seleccione la Marca">
                    <option value="0">Seleccione</option>
                    <?php
                    include './listas/selectMarca.php';
                    cuentaCaracteres
                    ?>
                </select>
            </li> 
            <li>
                <label for="modelo">Modelo:</label>
                <input type="text" id="modelo" name="modelo" value='<?php echo $modelo; ?>' required/>
            </li> 
            <li>
                <label for="idcolor">Color:</label>
                <select id="idcolor" name="idcolor" title="Seleccione el color">
                    <option value="0">Seleccione</option>
                    <?php
                    include './listas/selectColor.php';
                    ?>
                </select>
            </li> 
            <li>
                <label for="placa">Placa:</label>
                <input type="text" id="placa" name="placa" value='<?php echo $placa; ?>' required/>
            </li>
            <li>
                <label for="precio">Precio:</label>
                <b style="font-size: 17px; color: black;">$/.</b> <input type="text" id="precio" name="precio" value='<?php echo "" . $precio; ?>' required/>
            </li>
            <li>
                <label for="descripcion">Descripcion:</label>
                <textarea id="descripcion" name="descripcion" rows="5"  style="resize: none;" onkeypress="return limita(143);" value='<?php echo $descripcion; ?>'></textarea> 
            </li> 
            <li>
                <label for="anio">Año: </label>
                <select id="anio" name="anio" title="Seleccione el Año">
                    <option value="0">Año</option>
                    <?php
                    for ($i = 1960; $i <= 2017; $i++) {
                        echo "<option value='$i' ";
                        if ($anio == $i) {
                            echo "selected ";
                        }
                        echo ">$i</option>";
                    }
                    ?>
                </select> 
            </li>  
            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)">Modificar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoAutos.php')">Regresar</button> 
                <input type="hidden" name="form" value="actualizarAuto">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </li>
        </ul>
    </form>
<?php } else { ?>
    <form class="contact_form" action="../../sisWeb/model/insertar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Registro de un nuevo Auto</h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <li>
                <label for="idmarca">Marca:</label>
                <select id="idmarca" name="idmarca" title="Seleccione la Marca">
                    <option value="0">Seleccione</option>
                    <?php
                    include './listas/selectMarca.php';
                    cuentaCaracteres
                    ?>
                </select>
            </li> 
            <li>
                <label for="modelo">Modelo:</label>
                <input type="text" id="modelo" name="modelo" placeholder="Ingrese el modelo" required/>
            </li> 
            <li>
                <label for="idcolor">Color:</label>
                <select id="idcolor" name="idcolor" title="Seleccione el color">
                    <option value="0">Seleccione</option>
                    <?php
                    include './listas/selectColor.php';
                    ?>
                </select>
            </li> 
            <li>
                <label for="placa">Placa:</label>
                <input type="text" id="placa" name="placa" placeholder="Ingrese la placa" required/>
            </li>
            <li>
                <label for="precio">Precio:</label>
                <b style="font-size: 17px; color: black;">$/.</b> <input type="text" id="precio" name="precio" placeholder="Igrese el precio" required/>
            </li>
            <li>
                <label for="descripcion">Descripcion:</label>
                <textarea id="descripcion" name="descripcion" rows="5"  style="resize: none;" onkeypress="return limita(143);" placeholder="Ingrese la descripcion" ></textarea> 
            </li> 
            <li>
                <label for="anio">Año: </label>
                <select id="anio" name="anio" title="Seleccione el Año">
                    <option value="0">Año</option>
                    <?php
                    for ($i = 1960; $i <= 2017; $i++) {
                        echo "<option value='$i' ";
                        if ($anio == $i) {
                            echo "selected ";
                        }
                        echo ">$i</option>";
                    }
                    ?>
                </select> 
            </li>  
            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)">Registrar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoAutos.php')">Cancelar</button> 
                <input type="hidden" name="form" value="registrarAuto">
            </li>
        </ul>
    </form>

    <?php
}
include 'footer.php';
