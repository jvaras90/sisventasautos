<?php
include '../model/crearConexion.php';
//include './select.php';
$conexion = conectarse();
session_start();
clearstatcache();

if ($_SESSION['idpersonal'] == "") {
    header("Location: ../index.php");
    exit();
}
$day = date('Y-m-d');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <title>Sistema de Ventas | Lyonne Autos</title>
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!------------------- Styles ------------------->
        <link rel="stylesheet" href="../styles/layout.css" type="text/css" media="all">
        <link rel="stylesheet" href="../styles/mediaqueries.css" type="text/css" media="all">        
        <link rel="stylesheet" href="../styles/datatables.css" type="text/css" media="all"/>
        <link rel="stylesheet" href="../styles/form.css" type="text/css" media="all"> 

        <!------------------- Styles ------------------->
        <script src="../js/jquery.1.9.0.min.js" type="text/javascript"></script> 
        <script src="../js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../js/table.js"></script>      
        <style type="text/css">
            div.full_width{margin-top:20px;}
            div.full_width:first-child{margin-top:0;}
            div.full_width div{color:#666666; background-color:#DEDEDE;}
        </style>
        <!-- END DEMO STYLING -->
    </head>
    <body>
        <div class="seccion titulo">
            <header id="header" class="clear">
                <div id="hgroup">
                    <h1><a href="#">Sistema de Ventas | Lyonne Autos</a></h1>
                    <h2>Aplicativo WEB con Base de Datos</h2>
                </div>
                <div style="text-align: right;margin: 12px 80px 0px 0px;font-weight: bold">
                    <?php echo $_SESSION['nombre'] . "<br>"; ?>
                    <?php echo $_SESSION['cargo'] . "<br>"; ?>
                </div>
            </header>
        </div>
        <!-- ################################################################################################ -->
        <div class="seccion menu">
            <nav id="topnav">
                <ul class="clear">
                    <li class="active first"><a href="../index.php">Bienvenida</a></li>

                    <li><a href="" class="drop">Mantenimientos</a>
                        <ul>
                            <li  title="Listado de CLientes"><a class="drop" href="listadoClientes.php">Clientes</a>
                                <ul class="negative"> 
                                    <li title="Agregar un nuevo Cliente"><a href="gestCliente.php" >Registro</a></li>
                                </ul>
                            </li>
                            <li title="Listado de Colaboradores"><a class="drop" href="listadoColaboradores.php">Colaboradores</a>
                                <ul class="negative"> 
                                    <li title="Agregar un nuevo colaborador"><a href="gestColaborador.php">Registro</a></li>
                                </ul>
                            </li>
                            <li title="Listado de Automóviles"><a class="drop" href="listadoAutos.php">Automóviles</a>
                                <ul class="negative"> 
                                    <li title="Agregar un nuevo automóvil"><a href="gestAuto.php">Registro</a></li>
                                </ul>
                            </li>
                            <li title="Listado de Bonos"><a class="drop" href="listadoBonos.php">Bonos</a>
                                <ul class="negative"> 
                                    <li title="Agregar un nuevo automóvil"><a href="gestBono.php">Registro</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li> 
                    <li><a href="" class="drop">Ventas y Servicios</a>
                        <ul>
                            <li><a href="registrarVentas.php?tipo_c=1">Registrar Venta</a> </li>
                            <li><a href="listadoVentas.php?tipo_c=1">Listar Ventas</a> </li>
                            <li><a href="listadoCompAnulados.php">Comprobantes Anulados</a> </li>
                        </ul>
                    </li>
                    <!--                    <li class="last-child"><a class="drop" href="">Drop 2</a>
                                            <ul>
                                                <li><a href="#">Level 2</a></li>
                                                <li><a href="#">Level 2</a></li>
                                                <li><a class="drop" href="#">Level 2 + Drop</a>
                                                    <ul class="negative">
                                                        <li><a href="#">Level 3</a></li>
                                                        <li><a href="#">Level 3</a></li>
                                                        <li><a href="#">Level 3</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li> -->
                    <li><a href="../model/logout.php" title="Desloguearse del sistema">Salir</a></li>
                </ul>
            </nav>
        </div>

        <!-- content -->
        <div class="seccion contenido">
            <div id="container">