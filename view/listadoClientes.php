
<?php
include 'header.php';
?>
<h1 class="cabeTitulo a-center">Listado de Clientes</h1>
<table id="tabla" class="display" cellspacing="0"  style="margin: auto">
    <thead>
        <tr class="headings">
            <th class="a-center">
                #
            </th>
            <th class="column-title">Nombres</th>
            <th class="column-title">Dni</th>
            <th class="column-title">Fecha Nacimiento</th>
            <th class="column-title">Teléfono</th>
            <th class="column-title">E-mail</th>
            <th class="column-title">Domicilio</th> 
            <th class="column-title">Estado</th>
            <th class="column-title">Acción</th> 
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $sql = "select p.*,  ruc  ,p.idestado , e.descripcion estado
            from persona p 
            inner join cliente c on  c.idpersona = p.idpersona 
            inner join estado e on e.idestado = p.idestado ;";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $id = $row['idpersona'];
            $nombres = $row['nombres'];
            $apaterno = $row['apaterno'];
            $amaterno = $row['amaterno'];
            $fllname = "$nombres $apaterno $amaterno";
            $dni = $row['dni'];
            $fecha_nacimiento = $row['fecha_nacimiento'];
            $telefono = $row['telefono'];
            $email = $row['email'];
            $domicilio = $row['domicilio'];
            $idestado = $row['idestado'];
            $estado = $row['estado'];
            ?>
            <tr class="even pointer"> 
                <td class="column-row"><?php echo $i ?></td>
                <td class="column-row"><?php echo "$fllname"; ?></td>
                <td class="column-row"><?php echo "$dni"; ?></td>
                <td class="column-row"><?php echo "$fecha_nacimiento"; ?></td>
                <td class="column-row"><?php echo "$telefono"; ?></td>
                <td class="column-row"><?php echo "$email"; ?></td>
                <td class="column-row"><?php echo "$domicilio"; ?></td> 
                <td class="column-row"><?php echo "$estado"; ?></td>
                <td class="column-last a-center"> 
                    <a href="gestCliente.php?id=<?php echo $id ?>" class="boton verde" >Editar</a>
                    
                    <?php if ($idestado != 2) { ?>
                    <button type="button" class="boton rojo" title="Quitar" onclick="quitarElemento('clientes ',<?php echo "'$fllname'"; ?>, 'persona', 'idpersona',<?php echo $id ?>, 'listadoClientes.php', 'quitarElemento')"><b> - </b></button>
                    <?php } ?>
                </td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </tbody>
</table>

<?php
include 'footer.php';
