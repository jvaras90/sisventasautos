
<?php
include 'header.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "select p.*, YEAR(fecha_nacimiento) anio, Month(fecha_nacimiento)mes, 
            day(fecha_nacimiento) dia, ruc from persona p 
            inner join cliente c on  c.idpersona = p.idpersona where p.idpersona=$id;";
    //echo $sql;
    $result = mysql_query($sql, $conexion);
    while ($row = mysql_fetch_array($result)) {
        $nombres = $row['nombres'];
        $apaterno = $row['apaterno'];
        $amaterno = $row['amaterno'];
        $dni = $row['dni'];
        $telefono = $row['telefono'];
        $email = $row['email'];
        $domicilio = $row['domicilio'];
        $idestado = $row['idestado'];
        $ruc = $row['ruc'];
        $anio = $row['anio'];
        $mes = $row['mes'];
        $dia = $row['dia'];
    }
    ?>

    <form class="contact_form" action="../../sisWeb/model/modificar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Modificar los datos de <?php echo "<b>$nombres $apaterno $amaterno</b>"; ?></h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <li>
                <label for="nombres">Nombres:</label>
                <input type="text" id="nombres" name="nombres" value='<?php echo $nombres; ?>' required/>
            </li>
            <li>
                <label for="apaterno">A. Paterno:</label>
                <input type="text" id="apaterno" name="apaterno" value='<?php echo $apaterno; ?>' required/>
            </li>
            <li>
                <label for="amaterno">A. Materno:</label>
                <input type="text" id="amaterno" name="amaterno" value='<?php echo $amaterno; ?>' required/>
            </li>
            <li>
                <label for="dni">DNI: </label>
                <input type="text" id="dni" name="dni" value='<?php echo $dni; ?>' required/>
            </li>
            <li>
                <label for="anio">Fecha Nacimiento: </label>
                <select id="anio" name="anio" title="Seleccione el Año">
                    <option value="0">Año</option>
                    <?php
                    for ($i = 1960; $i <= 2017; $i++) {
                        echo "<option value='$i' ";
                        if ($anio == $i) {
                            echo "selected ";
                        }
                        echo ">$i</option>";
                    }
                    ?>
                </select> 
                <select id="mes" name="mes" title="Seleccione el Mes">
                    <option value="0">Mes</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        echo "<option value='$i' ";
                        if ($mes == $i) {
                            echo "selected ";
                        }
                        echo ">$i</option>";
                    }
                    ?>
                </select>
                <select id="dia" name="dia" title="Seleccione el Día">
                    <option value="0">Dia</option>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        echo "<option value='$i' ";
                        if ($dia == $i) {
                            echo "selected ";
                        }
                        echo ">$i</option>";
                    }
                    ?>
                </select>
            </li> 
            <li>
                <label for="ruc">Ruc: </label>
                <input type="text" id="ruc" name="ruc" value='<?php echo $ruc; ?>' />
            </li>
            <li>
                <label for="telefono">Teléfono: </label>
                <input type="text" id="telefono" name="telefono" value='<?php echo $telefono; ?>' />
            </li>
            <li>
                <label for="domicilio">Dirección: </label>
                <input type="text" id="domicilio" name="domicilio" value='<?php echo $domicilio; ?>' />
            </li>
            <li>
                <label for="email">Email:</label>
                <input type="email" name="email" id="email"value='<?php echo $email; ?>' />
                <span class="form_hint">Formato Apropiado "nombre@dominio.com"</span>
            </li> 
            <li>
                <label for="idestado">Estado:</label>
                <select id="idestado" name="idestado" title="Seleccione el Estado">
                    <option value="0">Seleccione</option>
                    <?php
                    include './listas/selectEstado1.php';
                    ?>
                </select>
            </li> 
            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)" >Modificar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoClientes.php')" >Regresar</button> 
                <input type="hidden" name="form" value="actualizarCliente">
                <input type="hidden" name="id" value="<?php echo $id; ?>"> 

            </li>
        </ul>
    </form>
<?php } else { ?>
    <form class="contact_form" action="../../sisWeb/model/insertar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Registro de Cliente</h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <li>
                <label for="nombres">Nombres:</label>
                <input type="text" id="nombres" name="nombres" placeholder="Ingrese nombres" required/>
            </li>
            <li>
                <label for="apaterno">A. Paterno:</label>
                <input type="text" id="apaterno" name="apaterno" placeholder="Ingrese apellido paterno" required/>
            </li>
            <li>
                <label for="amaterno">A. Materno:</label>
                <input type="text" id="amaterno" name="amaterno"  placeholder="Ingrese apellido materno" required/>
            </li>
            <li>
                <label for="dni">DNI: </label>
                <input type="text" id="dni" name="dni" placeholder="Igrese documento de identidad" required/>
            </li>
            <li>
                <label for="anio">Fecha Nacimiento: </label>
                <select id="anio" name="anio" title="Seleccione el Año">
                    <option value="0">Año</option>
                    <?php
                    for ($i = 1960; $i <= 2017; $i++) {
                        echo "<option value='$i' ";
                        echo ">$i</option>";
                    }
                    ?>
                </select> 
                <select id="mes" name="mes" title="Seleccione el Mes">
                    <option value="0">Mes</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        echo "<option value='$i' ";
                        echo ">$i</option>";
                    }
                    ?>
                </select>
                <select id="dia" name="dia" title="Seleccione el Día">
                    <option value="0">Dia</option>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        echo "<option value='$i' ";
                        echo ">$i</option>";
                    }
                    ?>
                </select>
            </li> 
            <li>
                <label for="ruc">Ruc: </label>
                <input type="text" id="ruc" name="ruc" placeholder="Ingrese Ruc"  />
            </li>
            <li>
                <label for="telefono">Teléfono: </label>
                <input type="text" id="telefono" name="telefono" placeholder="Ingrese Teléfono" />
            </li>
            <li>
                <label for="domicilio">Dirección: </label>
                <input type="text" id="domicilio" name="domicilio" placeholder="Ingrese dirección" />
            </li>
            <li>
                <label for="email">Email:</label>
                <input type="email" name="email" id="email"  placeholder="Ingrese su correo electrónico" />
                <span class="form_hint">Formato Apropiado "nombre@dominio.com"</span>
            </li> 
            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)">Registrar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoClientes.php')">Cancelar</button> 
                <input type="hidden" name="form" value="registrarCliente">
            </li>
        </ul>
    </form>
    <?php
}
include 'footer.php';
