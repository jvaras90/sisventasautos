<?php
include 'mensajesAlerta.php';
?>
<div style="height: 300px;"></div>
</div>
</div>

<!-- Footer -->
<div class="wrapper row4">
    <footer id="footer" class="clear">
        <p class="fl_left">Copyright &copy; 2017 - All Rights Reserved - <a href="#"> Universidad Ricardo Palma</a></p>
        <p class="fl_right"><a href="#" title="">DISEÑO Y PROGRAMACION WEB</a></p>
    </footer>
</div>
<script>
    function quitarElemento(lista, nombreElemento, table, atributoFK, id, pag,form)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombreElemento + '" de la lista de ' + lista + ' \n¿Desea Continuar?'))
        {
            $.post('../model/modificar.php', {form: form, tabla: table, id: id, pagina: pag,atributo: atributoFK}); 
            window.location.href = '' + pag+'?msj=6';
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>
<script>
    function enviarForm(boton) {
        var anio = $('#anio').val();
        var mes = $('#mes').val();
        var dia = $('#dia').val();
        var idcargo = $('#idcargo').val();
        var idmarca = $('#idmarca').val();
        var idcolor = $('#idcolor').val();
        var idcliente = $('#idcliente').val();
        var motivo = $('#motivo').val();

        if (anio == '0') {
            alert('Seleccione el Año');
            $('#anio').focus();
            return false;
        } else if (mes == '0') {
            alert('Seleccione el Mes de nacimiento de la persona');
            $('#mes').focus();
            return false;
        } else if (dia == '0') {
            alert('Seleccione el Dia de nacimiento de la persona');
            $('#dia').focus();
            return false;
        } else if (idcargo == '0') {
            alert('Seleccione el Cargo');
            $('#idcargo').focus();
            return false;
        } else if (idmarca == '0') {
            alert('Seleccione la Marca');
            $('#idmarca').focus();
            return false;
        } else if (idcolor == '0') {
            alert('Seleccione el Color');
            $('#idcolor').focus();
            return false;
        } else if (idcliente == '0') {
            alert('Seleccione un Cliente');
            $('#idcliente').focus();
            return false;
        } else if (motivo == '') {
            alert('Ingrese el Motivo por el cual desea anular el comprobante');
            $('#motivo').focus();
            return false;
        } else {
            boton.disabled = true;
            boton.value = 'Registrando...';
            boton.form.submit();
        }
    }
    function redirectForm(ruta) {
        window.location = "" + ruta + "";
        //return false;
    }
<?php if (isset($_GET['id'])) { ?>
        $(function () {
            var password = document.getElementById("clave2"), confirm_password = document.getElementById("clave3");
            function validatePassword() {
                if (password.value != confirm_password.value) {
                    confirm_password.setCustomValidity("Las contraseñas no coinciden");
                } else {
                    confirm_password.setCustomValidity('');
                }
            }
            password.onchange = validatePassword;
            confirm_password.onkeyup = validatePassword;
        });

<?php } ?>
</script>
<!--<script>
    jQuery.datepicker.regional['eu'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['eu']);
    $(function () {
        $("#dia").datepicker();
        $("#fecDesde").datepicker();
        $("#fecHasta").datepicker();
        $("#gesDesde").datepicker();
        $("#gesHasta").datepicker();
    });
</script>-->
</body>
</html>