
<?php
include 'header.php';
?>
<h1 class="cabeTitulo a-center">Comprobantes Anulados</h1>
<table id="tabla" class="display" cellspacing="0"  style="margin: auto">
    <thead>
        <tr class="headings">
            <th class="a-center">
                #
            </th>
            <th class="column-title">Cliente</th>
            <th class="column-title">Tipo Comp.</th>
            <th class="column-title">Motivo</th> 
            <th class="column-title">Vendedor</th>
            <th class="column-title">Fecha</th> 
            <th class="column-title">Hora</th> 
            <th class="column-title">Acción</th> 
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $sql = "Select fc.idfac_cabe,DATE_FORMAT(fc.fecha_emision, '%d-%m-%Y') fecha_emision,DATE_FORMAT(fc.fecha_emision, '%H:%i:%s') hora_emision,
            fc.idcliente, concat(p.nombres,' ',p.apaterno,' ',p.amaterno) cliente, fc.total, fc.tipo_c , tc.descripcion comprobante ,
 concat(pe.nombres,' ',pe.apaterno) colaborador, fc.idestado, e.descripcion estado, motivo
 from fac_cabe fc
 inner join cliente c on c.idcliente = fc.idcliente
 inner join persona p on p.idpersona = c.idpersona
 inner join tipo_comprobante tc on tc.idtipo_c = fc.tipo_c
 inner join colaborador col on col.idcolaborador = fc.idcolaborador
 inner join persona pe on pe.idpersona = col.idpersona
 inner join estado e on e.idestado = fc.idestado
 inner join anulaciones an on an.idfac_cabe = fc.idfac_cabe
 where p.idestado = 1 and  fc.idestado = 8
 order by fecha_emision desc,hora_emision desc;";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $id = $row['idfac_cabe'];
            $fecha_emision = $row['fecha_emision'];
            $hora_emision = $row['hora_emision'];
            $cliente = $row['cliente'];
            $total = $row['total'];
            $comprobante = $row['comprobante'];
            $colaborador = $row['colaborador'];
            $motivo = $row['motivo'];
            $estado = $row['estado'];
            $idestado = $row['idestado'];
            ?>
            <tr class="even pointer"> 
                <td class="column-row"><?php echo $i ?></td>
                <td class="column-row"><?php echo "$cliente"; ?></td>
                <td class="column-row"><?php echo "$comprobante"; ?></td>
                <td class="column-row"><?php echo "$motivo"; ?></td> 
                <td class="column-row"><?php echo "$colaborador"; ?></td>
                <td class="column-row"><?php echo "$fecha_emision"; ?></td> 
                <td class="column-row"><?php echo "$hora_emision"; ?></td> 
                <td class="column-last a-center"> 
                    <a href="gestVenta.php?id=<?php echo $id ?>" class="boton verde" >Ver Detalle</a> 
                </td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </tbody>
</table>

<?php
include 'footer.php';
