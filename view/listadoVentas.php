
<?php
include 'header.php';
?>
<h1 class="cabeTitulo a-center">Listado de Ventas</h1>
<table id="tabla" class="display" cellspacing="0"  style="margin: auto">
    <thead>
        <tr class="headings">
            <th class="a-center">
                #
            </th>
            <th class="column-title">Cliente</th>
            <th class="column-title">Tipo Comp.</th>
            <th class="column-title">Numero</th>
            <th class="column-title">Estado</th>
            <th class="column-title">Total</th>
            <th class="column-title">Vendedor</th>
            <th class="column-title">Fecha</th> 
            <th class="column-title">Hora</th> 
            <th class="column-title">Acción</th> 
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $sql = " select fc.idfac_cabe,DATE_FORMAT(fc.fecha_emision, '%d-%m-%Y') fecha_emision,DATE_FORMAT(fc.fecha_emision, '%H:%i:%s') hora_emision,
            fc.idcliente, concat(p.nombres,' ',p.apaterno,' ',p.amaterno) cliente, fc.total, fc.tipo_c , tc.descripcion comprobante , fc.numero_comp,
            concat(pe.nombres,' ',pe.apaterno) colaborador, fc.idestado, e.descripcion estado
            from fac_cabe fc
            inner join cliente c on c.idcliente = fc.idcliente
            inner join persona p on p.idpersona = c.idpersona
            inner join tipo_comprobante tc on tc.idtipo_c = fc.tipo_c
            inner join colaborador col on col.idcolaborador = fc.idcolaborador
            inner join persona pe on pe.idpersona = col.idpersona
            inner join estado e on e.idestado = fc.idestado
            where p.idestado = 1
            order by fecha_emision desc,hora_emision desc;";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $id = $row['idfac_cabe'];
            $fecha_emision = $row['fecha_emision'];
            $hora_emision = $row['hora_emision'];
            $cliente = $row['cliente'];
            $total = $row['total'];
            $tipo_c = $row['tipo_c'];
            $comprobante = $row['comprobante'];
            $colaborador = $row['colaborador'];
            $numero_comp = $row['numero_comp'];
            $estado = $row['estado'];
            $idestado = $row['idestado'];
            ?>
            <tr class="even pointer"> 
                <td class="column-row"><?php echo $i ?></td>
                <td class="column-row"><?php echo "$cliente"; ?></td>
                <td class="column-row"><?php echo "$comprobante"; ?></td>
                <td class="column-row"><?php echo "$numero_comp"; ?></td>
                <td class="column-row"><?php echo "$estado"; ?></td>
                <td class="column-row"><?php echo "$total"; ?></td>
                <td class="column-row"><?php echo "$colaborador"; ?></td>
                <td class="column-row"><?php echo "$fecha_emision"; ?></td> 
                <td class="column-row"><?php echo "$hora_emision"; ?></td> 
                <td class="column-last a-center"> 
                    <a href="gestVenta.php?tipo_c=<?php echo $tipo_c; ?>&id=<?php echo $id ?>" class="boton verde" >Ver Detalle</a>
                    <?php if ($idestado == 5) { ?>
                        <a href="gestVenta.php?id=<?php echo $id ?>&idestadoC=8" class="boton rojo" >Anular</a> 
                        <!--<button type="button" class="boton rojo" title="Quitar" onclick="anularComp('bonos ',<?php echo "'$bono'"; ?>, 'bono', 'idbono',<?php echo $id ?>, 'listadoBonos.php', 'quitarElemento')"><b> Anular </b></button>-->

                    <?php } ?>
                </td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </tbody>
</table>

<script>
    function anularComp(lista, nombreElemento, table, atributoFK, id, pag, form)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombreElemento + '" de la lista de ' + lista + ' \n¿Desea Continuar?'))
        {
            $.post('../model/modificar.php', {form: form, tabla: table, id: id, pagina: pag, atributo: atributoFK});
            window.location.href = '' + pag + '?msj=6';
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>
<?php
include 'footer.php';
