
<?php
include 'header.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "select * from bono where idbono = $id;";
    //echo $sql;
    $result = mysql_query($sql, $conexion);
    while ($row = mysql_fetch_array($result)) {
        $descripcion = $row['descripcion'];
        $precio = $row['precio'];
        $idestado = $row['idestado'];
    }
    ?>

    <form class="contact_form" action="../../sisWeb/model/modificar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Modificar los datos de <?php echo "<b>$descripcion</b>"; ?></h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <li>
                <label for="descripcion">Bono:</label>
                <input type="text" id="descripcion" name="descripcion" value='<?php echo $descripcion; ?>' required/>
            </li>
            <li>
                <label for="precio">Precio:</label>
                <b style="font-size: 17px; color: black;">$/.</b> <input type="text" id="precio" name="precio" value='<?php echo "" . $precio; ?>' required/>
            </li>
            <li>
                <label for="idestado">Estado:</label>
                <select id="idestado" name="idestado" title="Seleccione el estado">
                    <option value="0">Seleccione</option>
                    <?php
                    include './listas/selectEstado1.php';
                    cuentaCaracteres
                    ?>
                </select>
            </li> 

            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)">Modificar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoBonos.php')">Regresar</button> 
                <input type="hidden" name="form" value="actualizarBono">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </li>
        </ul>
    </form>
<?php } else { ?>
    <form class="contact_form" action="../../sisWeb/model/insertar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Registro de un Nuevo Bono</h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <li>
                <label for="descripcion">Bono:</label>
                <input type="text" id="descripcion" name="descripcion"   required/>
            </li>
            <li>
                <label for="precio">Precio:</label>
                <b style="font-size: 17px; color: black;">$/.</b> <input type="text" id="precio" name="precio" required/>
            </li> 
            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)">Registrar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoAutos.php')">Cancelar</button> 
                <input type="hidden" name="form" value="registrarBono">
            </li>
        </ul>
    </form>

    <?php
}
include 'footer.php';
