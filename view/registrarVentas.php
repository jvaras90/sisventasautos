 
<?php
include 'header.php';
if (isset($_GET['tipo_c'])) {
    $tipo_c = $_GET['tipo_c'];
    if ($tipo_c == 1) {
        $sql = "select max(numero_comp) maxid from fac_cabe where tipo_c = 1;";
    } else {
        $sql = "select max(numero_comp) maxid from fac_cabe where tipo_c = 2;";
    }
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    $ultimo = $row['maxid'];
    $numberC = substr($ultimo, 1) + 1;
    
}
if (isset($_GET['idcliente'])) {
    $idcliente = $_GET['idcliente'];
}
?>   
<script>
    $(document).ready(function () {
        var lista = document.getElementById("tipo_c");
        var lista1 = document.getElementById("idcliente");
        $('#tipo_c').change(function () {
            window.location = "registrarVentas.php?tipo_c=" + lista.value + "&idcliente=" + lista1.value;
            //$('#tipo_c').attr('disabled');
        });
        $('#idcliente').change(function () {
            window.location = "registrarVentas.php?tipo_c=" + lista.value + "&idcliente=" + lista1.value;
        });
    });</script>
<form class="contact_form" action="../model/insertar.php" method="POST" style="background-color: #e5f7c5; padding: 15px; margin: 10px;">
    <div style="text-align: center; color: black; font-size: 15px;">  
        <div class="parte-12"> 
            <span><b>Tipo de Comprobante:&nbsp; </b></span> 
            <select name="tipo_c" id="tipo_c">
                <option value="0"> Seleccione </option>
                <option value="1" <?php
                if ($_GET['tipo_c'] == 1) {
                    echo "selected";
                }
                ?>>Boleta de Venta</option> 
                <option value="2" <?php
                if ($_GET['tipo_c'] == 2) {
                    echo "selected";
                }
                ?>>Factura</option>
            </select>
        </div><br><br>
        <div class="parte-8 a-left">
            <span><b>Fecha de Emision: <?php echo date("d") . " de " . date("m") . " del " . date("Y"); ?></b></span><br>
            <label><b>Cliente: </b></label >
            <select id="idcliente" name="idcliente" >
                <option value="0"> Seleccione un Cliente</option>
                <?php include './listas/selectCliente.php'; ?>
            </select> 
        </div> 
        <div class="parte-4">
            <span><b>R.U.C. 10444784523</b></span><br>
            <span><b><?php
                    if ($tipo_c == 2) {
                        echo "Factura";
                    } else {
                        echo "Boleta de Venta";
                    }
                    ?></b>
            </span><br> 
            <span><b>N° 00<?php echo "00$numberC"; ?></b></span>
            <input type="hidden" name="numero_c" id="numero_c" value="<?php if($tipo_c==1){echo "B00$numberC";} else { echo "F00$numberC";} ?>">
        </div><br><br><br> <br> 
        <div class="parte-12" style="border: solid 1px #001018; padding: 5px;" title="Busqueda del Auto">
            <legend> <b>Busqueda del Auto</b>
                <div class="parte-12">
                    <!-- LA MARCA APARECE AQUI-->
                    <span> Marca: </span>
                    <select id="idmarca" name="idmarca">
                        <option value="99">Seleccione</option>
                        <?php include './listas/selectMarca.php'; ?>
                    </select> &nbsp;&nbsp;
                    <!-- LA CANTIDAD APARECE AQUI-->
                    <span>Cantidad: </span> 
                    <select id="cantidad" name="cantidad">
                        <?php
                        for ($i = 1; $i <= 40; $i++) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                        ?>
                    </select> 
                    <button type="button" class="boton verde" onclick="agregaAuto()" title="Agregar Auto al detalle de ventas">Agregar</button> 
                </div>
                <div class="parte-12">
                    <!-- EL AUTO APARECE AQUI-->
                    <span>Buscar Auto: </span>  
                    <select id="idautos" name="idautos"> 
                        <option value="0">Seleccione</option>
                        <?php //include './listas/selectAuto.php';  ?>
                    </select> &nbsp;&nbsp;
                    <!-- EL PRECIO SE GUARDA AQUI-->
                    <input type="hidden" id="precio" name="precio">
                </div>
            </legend>
        </div> 
        <div class="clearfix"></div><br>
        <div class="parte-12" style="border: solid 1px #001018; padding: 5px;" title="Busqueda del Bono">
            <legend> <b>Agregar Bonos</b>
                <div class="parte-12"> 
                    <!-- EL BONO APARECE AQUI-->
                    <span>Buscar Bonos: </span>  
                    <select id="idbonos" name="idbonos"> 
                        <option value="0">Seleccione</option>
                        <?php include './listas/selectBono.php'; ?>
                    </select> &nbsp;&nbsp;
                    <!-- EL PRECIO SE GUARDA AQUI-->
                    <input type="hidden" id="precio2" name="precio2">
                    <!-- LA CANTIDAD APARECE AQUI-->
                    <span>Cantidad: </span> 
                    <select id="cantidad2" name="cantidad2">
                        <?php
                        for ($i = 1; $i <= 40; $i++) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                        ?>
                    </select> 
                    <button type="button" class="boton verde" onclick="agregarBono()" title="Agregar bonos al detalle">Agregar</button> 
                </div>
                <div class="parte-12">
                </div>
            </legend>
        </div> 
        <br><br><br><br>
        <br><br><br>
        <div style="text-align: center;">
            <h1><i><u>Detalle de Venta</u></i></h1>
        </div> 
        <br>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Detalle de la Venta">
            <table id="table_boleta" class="a-center" style="background-color: white">
                <thead style="text-align: center;" >
                    <tr class="headings"> 
                        <th style="text-align: center;" class="column-title">Cantidad</th>
                        <th style="text-align: center;" class="column-title">Marca</th>
                        <th style="text-align: center;" class="column-title">Descripcion</th>
                        <th style="text-align: center;" class="column-title">Precio Unitario</th>
                        <th style="text-align: center;" class="column-title">Importe</th>
                        <th style="text-align: center;" class="hidden">Precio U.</th>
                        <th style="text-align: center;" class="hidden">Importe U</th>
                        <th style="text-align: center;width: 10px;" class="column-title">Gestión</th> 
                    </tr>
                </thead>
                <tbody style="text-align: center;"  > 
                </tbody>
                <?php if ($tipo_c == 2) { ?>
                    <tr class="a-rigth"> 
                        <td  style="background-color: white;border: none;" colspan="3"></td>
                        <td class="a-rigth"><input type="hidden" id="montoSubTotal"name="montoSubTotal"> Sub-Total $. </td>                        
                        <td class="a-center" id="subTotal">0.00</td> 
                    </tr>
                    <tr > 
                        <td  style="background-color: white;border: none;" colspan="3"></td>
                        <td class="a-rigth"><input type="hidden" id="montoIGV"name="montoIGV"> I.G.V 18 % </td>                        
                        <td class="a-center"  id="precioIgv">0.00</td> 
                    </tr>
                <?php } ?>
                <tr > 
                    <td  style="background-color: white;border: none;"  colspan="3"></td>
                    <td class="a-rigth" ><input type="hidden" id="monto_total" name="monto_total"> Total $. </td>                    
                    <td class="a-center" id="totalT">0.00</td> 
                </tr>
            </table>
            <table>
            </table>
        </div>
    </div>
    <div class="col-md-12 a-center">  
        <button type="button" id="btnRegistrar" class="hidden" onclick="enviarForm(this)" >Registrar</button>
        <button type="button" class="boton rojo" onclick="redirectForm('listadoVentas.php')">Cancelar</button>
        <input type="hidden" name="form" value="registrarVenta">
    </div>
</form>
<script type="text/javascript">
    $(function () {
        $("#idautos").change(function () {
            $.post('obtenerDatos.php', {idautos: $("#idautos").val()}, function (data) {
                $("#precio").val(data);
            });
        });
        $("#idbonos").change(function () {
            $.post('obtenerDatos.php', {idbonos: $("#idbonos").val()}, function (data) {
                $("#precio2").val(data);
            });
        });
        $("#idmarca").change(function () {
            $.post('obtenerDatos.php', {idmarca: $("#idmarca").val()}, function (data) {
                document.getElementById("idautos").innerHTML = data;
            });
        });



    });
    function agregaAuto() {
        $("#btnRegistrar").removeClass();
        $("#btnRegistrar").addClass("boton azul");
        //capturamos los valores
        var idmarca = $('#idmarca').val();
        var idcliente = $('#idcliente').val();
        var idauto = $('#idautos').val();
        var precio = $('#precio').val();

        var marca = $("#idmarca option:selected").html();
        var auto = $("#idautos option:selected").html();
        var cantidadN = $("#cantidad option:selected").html();

        var cat = document.getElementById("table_boleta").rows.length;

        var importe = (cantidadN * precio);

        if (idcliente == 0) {
            alert("Seleccione un Cliente");
            $('#idcliente').focus();
        } else if (idauto == 0) {
            alert("Seleccione una Auto");
            $('#idautos').focus();
        } else if (idmarca == 99) {
            alert("Seleccione una marca");
            $('#idmarca').focus();
        } else {
            if (idmarca > 0 && idauto > 0) {
                $('#table_boleta').append(
                        '<tr id="' + cat + '" class="impreso">\n\
                <td style="text-align: center;"><input type="hidden" name="cantidadR' + idauto + '" value="' + cantidadN + '"/>' + cantidadN + '</td>\n\
                <td style="text-align: center;">' + marca + '</td>\n\
                <td style="text-align: center;"><input type="hidden" name="idautosR' + idauto + '" value="' + idauto + '"/>' + auto + '</td>\n\
                <td style="text-align: center;">' + precio + '</td>\n\
                <td style="text-align: center;"><input type="hidden" name="importeR' + idauto + '" value="' + importe.toFixed(2) + '"/>' + importe.toFixed(2) + '</td>\n\
                <td style="text-align: center;" class="hidden">' + precio + '</td>\n\
                <td style="text-align: center;" class="hidden">' + importe + '</td>\n\
                <td style="text-align: center;"><button type="button" class="boton rojo" style="width:30px;margin:0px;padding:0px;" onclick="eliminarAgente(' + cat + ');" title="Quitar el Producto"><i class="fa fa-minus-square"></i> - </button></td>\n\
            </tr>\n\
            ');
            }
            calcularMonto();
            //limiamos los textbox
            $('#idmarca').val("");
            $('#idautos').val("");
            $('#cantidad').val("");
            $('#precio').val("");
        }
    }

    function agregarBono() {
        //capturamos los valores
        var idbonos = $('#idbonos').val();
        var precio = $('#precio2').val();

        var bono = $("#idbonos option:selected").html();
        var cantidadN2 = $("#cantidad2 option:selected").html();

        var cat = document.getElementById("table_boleta").rows.length;

        var importe = (cantidadN2 * precio);

        if (idbonos == 0) {
            alert("Seleccione un Bono");
            $('#idbonos').focus();
        } else {
            if (idbonos > 0) {
                $('#table_boleta').append(
                        '<tr id="' + cat + '" class="impreso">\n\
                <td style="text-align: center;"><input type="hidden" name="cantBonoR' + idbonos + '" value="' + cantidadN2 + '"/>' + cantidadN2 + '</td>\n\
                <td style="text-align: center;">Bono</td>\n\
                <td style="text-align: center;"><input type="hidden" name="idBonoR' + idbonos + '" value="' + idbonos + '"/>' + bono + '</td>\n\
                <td style="text-align: center;">' + precio + '</td>\n\
                <td style="text-align: center;"><input type="hidden" name="impoBonoR' + idbonos + '" value="' + importe.toFixed(2) + '"/>' + importe.toFixed(2) + '</td>\n\
                <td style="text-align: center;" class="hidden">' + precio + '</td>\n\
                <td style="text-align: center;" class="hidden">' + importe + '</td>\n\
                <td style="text-align: center;"><button type="button" class="boton rojo" style="width:30px;margin:0px;padding:0px;" onclick="eliminarAgente(' + cat + ');" title="Quitar el Producto"><i class="fa fa-minus-square"></i> - </button></td>\n\
            </tr>\n\
            ');
            }
            calcularMonto();
            //limiamos los textbox
            $('#idbonos').val("");
            $('#precio2').val("");
            $('#cantidad').val("");
        }
    }
    function calcularMonto() {
        var total2 = 0.00;
        $("#table_boleta tr").find('td:eq(6)').each(function () {
            var valor1 = $(this).html();
            total2 += parseFloat(valor1);
        });
<?php
if ($tipo_c == 2) {
    ?>
            var subtotal, igv, total = 0.00;
            subtotal = total2;
            igv = subtotal * parseFloat(0.18);
            total = subtotal + igv;
            $('#subTotal').text(subtotal.toFixed(2));
            $('#precioIgv').text(igv.toFixed(2));
            $('#totalT').text(total.toFixed(2));

            document.getElementById("montoSubTotal").value = subtotal.toFixed(2);
            document.getElementById("montoIGV").value = igv.toFixed(2);
            document.getElementById("monto_total").value = total.toFixed(2);
<?php } else { ?>
            $('#totalT').text(total2.toFixed(2));
            document.getElementById("monto_total").value = total2.toFixed(2);
<?php } ?>
    }
    function eliminarAgente(id)
    {
        $('#' + id).remove();
        calcularMonto();
    }
</script>
<?php
include 'footer.php';
