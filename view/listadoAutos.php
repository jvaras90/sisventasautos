
<?php
include 'header.php';
?>
<h1 class="cabeTitulo a-center">Listado de Autos</h1>
<table id="tabla" class="display" cellspacing="0"  style="margin: auto">
    <thead>
        <tr class="headings">
            <th class="a-center">
                #
            </th>
            <th class="column-title">Marca</th>
            <th class="column-title">Modelo</th>
            <th class="column-title">Color</th>
            <th class="column-title">Placa</th>
            <th class="column-title">Año</th>
            <th class="column-title">Precio</th> 
            <th class="column-title">Estado</th>
            <th class="column-title">Acción</th> 
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $sql = "select idautos, modelo, a.precio, a.placa, a.descripcion, a.anio , a.idmarca, m.descripcion marca, a.idcolor ,c.descripcion color , a.idestado , e.descripcion estado
                from autos a 
                inner join marca m on m.idmarca = a.idmarca
                inner join color c on c.idcolor = a.idcolor
                inner join estado e on e.idestado = a.idestado 
                order by marca;";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $id = $row['idautos'];
            $modelo = $row['modelo'];
            $precio = $row['precio'];
            $placa = $row['placa'];
            $anio = $row['anio'];
            $marca = $row['marca'];
            $color = $row['color'];
            $idestado = $row['idestado'];
            $estado = $row['estado'];
            //$ = $row['']; 
            ?>
            <tr class="even pointer"> 
                <td class="column-row"><?php echo $i ?></td>
                <td class="column-row"><?php echo "$marca"; ?></td>
                <td class="column-row"><?php echo "$modelo"; ?></td>
                <td class="column-row"><?php echo "$color"; ?></td>
                <td class="column-row"><?php echo "$placa"; ?></td>
                <td class="column-row"><?php echo "$anio"; ?></td>
                <td class="column-row"><?php echo "$/. $precio"; ?></td> 
                <td class="column-row"><?php echo "$estado"; ?></td>
                <td class="column-last a-center"> 
                    <a href="gestAuto.php?id=<?php echo $id ?>" class="boton verde" >Editar</a>                    
                    <?php if ($idestado != 2) { ?>
                    <button type="button" class="boton rojo" title="Quitar" onclick="quitarElemento('autos ',<?php echo "'$marca $modelo $color'"; ?>, 'autos', 'idautos',<?php echo $id ?>, 'listadoAutos.php','quitarElemento')"><b> - </b></button>
                    <?php } ?>
                </td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </tbody>
</table>



<?php
include 'footer.php';
