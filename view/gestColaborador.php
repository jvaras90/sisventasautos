
<?php
include 'header.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "select p.*, YEAR(fecha_nacimiento) anio, Month(fecha_nacimiento)mes, day(fecha_nacimiento) dia ,fecha_ingreso, fecha_desceso,idcargo,usuario
from persona p 
inner join colaborador c on  c.idpersona = p.idpersona 
inner join usuario u on u.idpersona = p.idpersona where p.idpersona=$id;";
    //echo $sql;
    $result = mysql_query($sql, $conexion);
    while ($row = mysql_fetch_array($result)) {
        $nombres = $row['nombres'];
        $apaterno = $row['apaterno'];
        $amaterno = $row['amaterno'];
        $dni = $row['dni'];
        $telefono = $row['telefono'];
        $email = $row['email'];
        $domicilio = $row['domicilio'];
        $idestado = $row['idestado'];
        $ruc = $row['ruc'];
        $anio = $row['anio'];
        $mes = $row['mes'];
        $dia = $row['dia'];
        $fecha_ingreso = $row['fecha_ingreso'];
        $fecha_desceso = $row['fecha_desceso'];
        $idcargo = $row['idcargo'];
        $usuario = $row['usuario'];
    }
    ?>

    <form class="contact_form" action="../../sisWeb/model/modificar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Modificar los datos de <?php echo "<b>$nombres $apaterno $amaterno</b>"; ?></h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <legend> Datos Personales<br>
                <li>
                    <label for="name">Nombres:</label>
                    <input type="text" id="nombres" name="nombres" value='<?php echo $nombres; ?>' required/>
                </li>
                <li>
                    <label for="name">A. Paterno:</label>
                    <input type="text" id="apaterno" name="apaterno" value='<?php echo $apaterno; ?>' required/>
                </li>
                <li>
                    <label for="name">A. Materno:</label>
                    <input type="text" id="amaterno" name="amaterno" value='<?php echo $amaterno; ?>' required/>
                </li>
                <li>
                    <label for="name">DNI: </label>
                    <input type="text" id="dni"s name="dni" value='<?php echo $dni; ?>' required/>
                </li>
                <li>
                    <label for="anio">Fecha Nacimiento: </label>
                    <select id="anio" name="anio" title="Seleccione el Año">
                        <option value="0">Año</option>
                        <?php
                        for ($i = 1960; $i <= 2017; $i++) {
                            echo "<option value='$i' ";
                            if ($anio == $i) {
                                echo "selected ";
                            }
                            echo ">$i</option>";
                        }
                        ?>
                    </select> 
                    <select id="mes" name="mes" title="Seleccione el Mes">
                        <option value="0">Mes</option>
                        <?php
                        for ($i = 1; $i <= 12; $i++) {
                            echo "<option value='$i' ";
                            if ($mes == $i) {
                                echo "selected ";
                            }
                            echo ">$i</option>";
                        }
                        ?>
                    </select>
                    <select id="dia" name="dia" title="Seleccione el Día">
                        <option value="0">Dia</option>
                        <?php
                        for ($i = 1; $i <= 31; $i++) {
                            echo "<option value='$i' ";
                            if ($dia == $i) {
                                echo "selected ";
                            }
                            echo ">$i</option>";
                        }
                        ?>
                    </select>
                </li> 
                <li>
                    <label for="telefono">Teléfono: </label>
                    <input type="text" id="telefono" name="telefono" value='<?php echo $telefono; ?>' required/>
                </li>
                <li>
                    <label for="domicilio">Dirección: </label>
                    <input type="text" id="domicilio" name="domicilio" value='<?php echo $domicilio; ?>' required/>
                </li>
                <li>
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email"value='<?php echo $email; ?>' required/>
                    <span class="form_hint">Formato Apropiado "nombre@dominio.com"</span>
                </li> 
                <li>
                    <label for="idestado">Estado:</label>
                    <select id="idestado" name="idestado" title="Seleccione el Estado">
                        <option value="0">Seleccione</option>
                        <?php
                        include './listas/selectEstado1.php';
                        ?>
                    </select>
                </li> 
            </legend>

            <legend> Datos De Acceso al Sistema<br>
                <li>
                    <label for="f_ingreso">Fecha de Ingreso:</label>
                    <input type="text"  name="f_ingreso" id="f_ingreso" value="<?php echo $fecha_ingreso; ?>" disabled=""/>
                    <span class="form_hint">Formato Apropiado "Año-Mes-Dia" </span>
                </li>
                <li>
                    <label for="f_cese">Fecha de Baja:</label>
                    <input type="text"  name="f_cese" id="f_cese" value="<?php
                    if ($fecha_desceso == "") {
                        echo "0000-00-00";
                    } else {
                        echo $fecha_desceso;
                    }
                    ?>" disabled=""/>
                    <span class="form_hint">Formato Apropiado "Año-Mes-Dia" </span>
                </li>
                <li>
                    <label for="idcargo">Cargo:</label>
                    <select id="idcargo" name="idcargo" title="Seleccione el Cargo">
                        <option value="0">Seleccione</option>
                        <?php
                        include './listas/selectCargo.php';
                        ?>
                    </select>
                </li> 
            </legend>
            <legend> Datos De Acceso al Sistema<br>
                <li>
                    <label for="name">Usuario: </label>
                    <input type="text" id="usuario" name="usuario" value='<?php echo $usuario; ?>'  required/>
                </li>
                <li>
                    <label for="clave1">Contraseña: </label>
                    <input type="password" id="clave1" name="clave1"  required />
                </li>
                <li>
                    <label for="clave2">Nueva Contraseña: </label>
                    <input type="password" id="clave2" name="clave2" />
                </li>
                <li title="Este Campo Se Utiliza Para Realizar Un Cambio de Contraseña al Usuario">
                    <label for="clave3">Rep. Nue. Contraseña: </label>
                    <input type="password" id="clave3" name="clave3" />
                    <span class="form_hint">Las Contraseñas no coinciden</span>
                </li>
            </legend>
            <li>
                <button type="button" class="boton azul"  onclick="enviarForm(this)">Modificar</button>
                <button type="button" class="boton rojo"  onclick="redirectForm('listadoColaboradores.php')">Regresar</button> 
                <input type="hidden" name="form" value="actualizarColaborador">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </li>
        </ul>
    </form>
<?php } else { ?>

    <form class="contact_form" action="../../sisWeb/model/insertar.php" method="post" name="contact_form">
        <ul>
            <li>
                <h2>Registro de Colaborador</h2>
                <span class="required_notification"><b>* Indica Campo Obligatorio</b></span>
            </li>
            <legend> Datos Personales<br>
                <li>
                    <label for="name">Nombres:</label>
                    <input type="text" id="nombres" name="nombres" value='<?php echo $nombres; ?>' required/>
                </li>
                <li>
                    <label for="name">A. Paterno:</label>
                    <input type="text" id="apaterno" name="apaterno" value='<?php echo $apaterno; ?>' required/>
                </li>
                <li>
                    <label for="name">A. Materno:</label>
                    <input type="text" id="amaterno" name="amaterno" value='<?php echo $amaterno; ?>' required/>
                </li>
                <li>
                    <label for="name">DNI: </label>
                    <input type="text" id="dni" name="dni" value='<?php echo $dni; ?>' required/>
                </li>
                <li>
                    <label for="anio">Fecha Nacimiento: </label>
                    <select id="anio" name="anio" title="Seleccione el Año">
                        <option value="0">Año</option>
                        <?php
                        for ($i = 1960; $i <= 2017; $i++) {
                            echo "<option value='$i' ";
                            if ($anio == $i) {
                                echo "selected ";
                            }
                            echo ">$i</option>";
                        }
                        ?>
                    </select> 
                    <select id="mes" name="mes" title="Seleccione el Mes">
                        <option value="0">Mes</option>
                        <?php
                        for ($i = 1; $i <= 12; $i++) {
                            echo "<option value='$i' ";
                            if ($mes == $i) {
                                echo "selected ";
                            }
                            echo ">$i</option>";
                        }
                        ?>
                    </select>
                    <select id="dia" name="dia" title="Seleccione el Día">
                        <option value="0">Dia</option>
                        <?php
                        for ($i = 1; $i <= 31; $i++) {
                            echo "<option value='$i' ";
                            if ($dia == $i) {
                                echo "selected ";
                            }
                            echo ">$i</option>";
                        }
                        ?>
                    </select>
                </li> 
                <li>
                    <label for="telefono">Teléfono: </label>
                    <input type="text" id="telefono" name="telefono" value='<?php echo $telefono; ?>' required/>
                </li>
                <li>
                    <label for="domicilio">Dirección: </label>
                    <input type="text" id="domicilio" name="domicilio" value='<?php echo $domicilio; ?>' required/>
                </li>
                <li>
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email"value='<?php echo $email; ?>' required/>
                    <span class="form_hint">Formato Apropiado "nombre@dominio.com"</span>
                </li>  
            </legend>

            <legend> Datos De Acceso al Sistema<br>
                <li>
                    <label for="f_ingreso">Fecha de Ingreso:</label>
                    <input type="text"  name="f_ingreso" id="f_ingreso" value="<?php echo date("Y-m-d"); ?>" disabled=""/>
                    <span class="form_hint">Formato Apropiado "Año-Mes-Dia" </span>
                </li> 
                <li>
                    <label for="idcargo">Cargo:</label>
                    <select id="idcargo" name="idcargo" title="Seleccione el Cargo">
                        <option value="0">Seleccione</option>
                        <?php
                        include './listas/selectCargo.php';
                        ?>
                    </select>
                </li> 
            </legend>
            <legend> Datos De Acceso al Sistema<br>
                <li>
                    <label for="name">Usuario: </label>
                    <input type="text" id="usuario" name="usuario" value='<?php echo $usuario; ?>'  required/>
                </li> 
                <li>
                    <label for="clave2">Contraseña: </label>
                    <input type="password" id="clave2" name="clave2" required/>
                </li>
                <li title="Este Campo Se Utiliza Para Realizar Un Cambio de Contraseña al Usuario">
                    <label for="clave3">Repita Contraseña: </label>
                    <input type="password" id="clave3" name="clave3" required/>
                    <span class="form_hint">Las Contraseñas no coinciden</span>
                </li>
            </legend>
            <li>
                <button class="boton azul"  onclick="enviarForm(this)">Registrar</button>
                <button class="boton rojo"  onclick="redirectForm('listadoColaboradores.php')">Cancelar</button> 
                <input type="hidden" name="form" value="registrarColaborador">
            </li>
        </ul>
    </form>


    <?php
}
include 'footer.php';
