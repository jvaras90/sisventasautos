
<?php
include 'header.php';
?>
<h1 class="cabeTitulo a-center">Listado de Bonos</h1>
<table id="tabla" class="display" cellspacing="0"  style="margin: auto">
    <thead>
        <tr class="headings">
            <th class="a-center">
                #
            </th>
            <th class="column-title">Bono</th>
            <th class="column-title">Precio</th>
            <th class="column-title">Estado</th>
            <th class="column-title">Ultima Act.</th> 
            <th class="column-title">Acción</th> 
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $sql = " select b.* , b.descripcion bono, b.idestado, e.descripcion estado 
            from bono b 
            inner join estado e on e.idestado = b.idestado
            where idbono not like '0';";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $id = $row['idbono'];
            $bono = $row['bono'];
            $precio = $row['precio'];
            $idestado = $row['idestado'];
            $estado = $row['estado'];
            $lastUpdated = $row['lastupdated'];
            ?>
            <tr class="even pointer"> 
                <td class="column-row"><?php echo $i ?></td>
                <td class="column-row"><?php echo "$bono"; ?></td>
                <td class="column-row"><?php echo "$precio"; ?></td>x
                <td class="column-row"><?php echo "$lastUpdated"; ?></td> 
                <td class="column-row"><?php echo "$estado"; ?></td>
                <td class="column-last a-center"> 
                    <a href="gestBono.php?id=<?php echo $id ?>" class="boton verde" >Editar</a> 
                    <?php if ($idestado != 2) { ?>
                        <button type="button" class="boton rojo" title="Quitar" onclick="quitarElemento('bonos ',<?php echo "'$bono'"; ?>, 'bono', 'idbono',<?php echo $id ?>, 'listadoBonos.php', 'quitarElemento')"><b> - </b></button>
                    <?php } ?>
                </td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </tbody>
</table>
<?php
include 'footer.php';
