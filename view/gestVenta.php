 
<?php
include 'header.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}


$sql = " select fc.idfac_cabe,DATE_FORMAT(fc.fecha_emision, '%d-%m-%Y') fecha_emision,DATE_FORMAT(fc.fecha_emision, '%h:%i:%s') hora_emision,
 fc.idcliente, concat(p.nombres,' ',p.apaterno,' ',p.amaterno) cliente, c.ruc, p.domicilio,p.email,
 fc.total, fc.tipo_c , tc.descripcion comprobante ,motivo,fc.numero_comp,
 concat(pe.nombres,' ',pe.apaterno) colaborador, fc.idestado, e.descripcion estado
 from fac_cabe fc
 inner join cliente c on c.idcliente = fc.idcliente
 inner join persona p on p.idpersona = c.idpersona
 inner join tipo_comprobante tc on tc.idtipo_c = fc.tipo_c
 inner join colaborador col on col.idcolaborador = fc.idcolaborador
 inner join persona pe on pe.idpersona = col.idpersona
 inner join estado e on e.idestado = fc.idestado
 left join anulaciones an on an.idfac_cabe = fc.idfac_cabe
 where p.idestado = 1 and fc.idfac_cabe = $id
 order by fecha_emision desc,hora_emision desc;";
//echo $sql;
$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
    $id = $row['idfac_cabe'];
    $fecha_emision = $row['fecha_emision'];
    $hora_emision = $row['hora_emision'];
    $idcliente = $row['idcliente'];
    $cliente = $row['cliente'];
    $ruc = $row['ruc'];
    $cliente = $row['cliente'];
    $domicilio = $row['domicilio'];
    $email = $row['email'];
    if (isset($_GET['tipo_c'])) {
        $tipo_c = $_GET['tipo_c'];
        if ($tipo_c == 1) {
            $sqlLN = "select max(numero_comp) maxid from fac_cabe where tipo_c = 1;";
        } else {
            $sqlLN = "select max(numero_comp) maxid from fac_cabe where tipo_c = 2;";
        }
        $res = mysql_query($sqlLN);
        $row1 = mysql_fetch_array($res);
        $ultimo = $row1['maxid'];
        $numberC = substr($ultimo, 1) + 1;
    } else {
        $tipo_c = $row['tipo_c'];
    }
    $comprobante = $row['comprobante'];
    $colaborador = $row['colaborador'];
    $numero_comp = $row['numero_comp'];
    $estado = $row['estado'];
    $motivo = $row['motivo'];

    if (isset($_GET['idestadoC'])) {
        $idestado = $_GET['idestadoC'];
    } else {
        $idestado = $row['idestado'];
    }
    //echo "estado $idestado";
}
?>   
<script>
    $(document).ready(function () {
        var lista = document.getElementById("tipo_c");
        $('#tipo_c').change(function () {
            window.location = "gestVenta.php?tipo_c=" + lista.value + "&id=<?php echo $id; ?>";
        });
    });
</script>
<form class="contact_form" action="../model/modificar.php" method="POST" style="background-color: #e5f7c5; padding: 4px; margin: 5px;">
    <div style="text-align: center; color: black; font-size: 15px; padding: 1px;">  
        <div class="parte-8 a-left"><br>
            <span><b>Fecha de Emision: <?php echo date("d", strtotime($fecha_emision)) . " de " . date("m", strtotime($fecha_emision)) . " del " . date("Y", strtotime($fecha_emision)); ?></b></span><br><br>
        </div> 
        <div class="parte-4" >
            <span><b>R.U.C. 10444784523</b></span><br>
            <span><b><?php
                    if ($tipo_c == 2) {
                        echo "Factura";
                    } else {
                        echo "Boleta de Venta";
                    }
                    ?></b>
            </span><br> 
            <span><b>N° <?php
                    if ($idestado == 6) {
                        echo $numero_comp;
                    } else {
                        echo "00$numberC";
                    }
                    ?></b></span>
            <input type="hidden" name="numero_c" id="numero_c" value="<?php
            if ($tipo_c == 1) {
                echo "B00$numberC";
            } else {
                echo "F00$numberC";
            }
            ?>">
        </div>
        <div class="clearfix"></div><br>
        <div class="parte-12"> 
            <div class="parte-6" >
                <span><b>Tipo de Comprobante:&nbsp; </b></span> 
                <select name="tipo_c" id="tipo_c" >
                    <option value="0"> Seleccione </option>
                    <option value="1" <?php
                    if ($tipo_c == 1) {
                        echo "selected";
                    }
                    ?>>Boleta de Venta</option> 
                    <option value="2" <?php
                    if ($tipo_c == 2) {
                        echo "selected";
                    }
                    ?>>Factura</option>
                </select>                
                <input type="hidden" id="tipo_c1" name="tipo_c1" value="<?php echo $_GET['tipo_c']; ?>">
            </div>
            <div class="parte-6" >

                <span><b>Estado del Comprobante:&nbsp; </b></span> 
                <select name="idestadoC" id="idestadoC" >  
                    <?php include './listas/selectEstado2.php'; ?>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>  
        <div class="parte-12">
            <br>
            <div class="parte-6">
                <label><b>Nombres: </b></label >
                <input type="text" name="cliente" value="<?php echo $cliente ?>" readonly >
            </div>
            <div class="parte-6">
                <label><b>Ruc: </b></label >
                <input type="text" name="cl1iente" value="<?php echo $ruc ?>" readonly >
            </div>
            <div class="clearfix"></div><br>
            <div class="parte-6">
                <label ><b>Direccion: </b></label >
                <input type="text" name="cliente" value="<?php echo $domicilio ?>" readonly >
            </div>
            <div class="parte-6">
                <label><b>E-Mail: </b></label>
                <input type="text" name="cliente" value="<?php echo $email ?>" readonly >
            </div>
            <div class="clearfix"></div> <br>
            <div class="parte-6"> 
                <button type="button" id="btnMostrarAutos" class="boton verde" onclick="habBusAuto()">Agregar Auto</button>
            </div>
            <div class="parte-6 a-left">
                <button type="button" id="btnMostrarBonos" class="boton verde" onclick="habBusBono()">Agregar Bono</button>
            </div> 
        </div> 
        <div class="clearfix"></div>
        <div id="motivoA" class="hidden" style="margin: 1px; padding: 10px;">
            <legend>
                <div class="parte-4 a-rigth">
                    <br>                
                    <span style="margin: 25px; padding: 10px 10px 10px 25px;"  >Motivo: </span>  &nbsp;&nbsp;<br>
                </div>
                <div class="parte-8 a-left">
                    <textarea style="resize: none;" name="motivo" id="motivo" placeholder="Ingrese Aqui el Motivo por el que anulará el Comprobante" required><?php
                        if (isset($_GET['idestadoC'])) {
                            echo "";
                        } else {
                            if ($estado == "Anulado") {
                                echo $motivo;
                            } else {
                                echo "1";
                            }
                        }
                        ?>
                    </textarea><br>
                </div>
            </legend>
            <b>~*~</b><br>
        </div>
        <div class="clearfix"></div><br>
        <div class="hidden" id="habilitarAutos" style="padding: 5px;" title="Busqueda del Auto">
            <legend> <b>Busqueda del Auto</b>
                <div class="parte-12">
                    <!-- LA MARCA APARECE AQUI-->
                    <span> Marca: </span>
                    <select id="idmarca" name="idmarca">
                        <option value="99">Seleccione</option>
                        <?php include './listas/selectMarca.php'; ?>
                    </select> &nbsp;&nbsp;
                    <!-- LA CANTIDAD APARECE AQUI-->
                    <span>Cantidad: </span> 
                    <select id="cantidad" name="cantidad">
                        <?php
                        for ($i = 1; $i <= 40; $i++) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                        ?>
                    </select> 
                    <button type="button" class="boton verde" onclick="agregaAuto()" title="Agregar Auto al detalle de ventas">Agregar</button> 
                </div>
                <div class="parte-12">
                    <!-- EL AUTO APARECE AQUI-->
                    <span>Buscar Auto: </span>  
                    <select id="idautos" name="idautos"> 
                        <option value="0">Seleccione</option> 
                    </select> &nbsp;&nbsp;
                    <!-- EL PRECIO SE GUARDA AQUI-->
                    <input type="hidden" id="precio" name="precio">
                </div>
            </legend>
            <b>~*~</b><br>
        </div>  

        <div class="clearfix"></div><br>
        <div class="hidden" id="habilitarBonos" style="padding: 5px;" title="Busqueda del Bono">
            <legend> <b>Agregar Bonos</b>
                <div class="parte-12"> 
                    <!-- EL BONO APARECE AQUI-->
                    <span>Buscar Bonos: </span>  
                    <select id="idbonos" name="idbonos"> 
                        <option value="0">Seleccione</option>
                        <?php include './listas/selectBono.php'; ?>
                    </select> &nbsp;&nbsp;
                    <!-- EL PRECIO SE GUARDA AQUI-->
                    <input type="hidden" id="precio2" name="precio2">
                    <!-- LA CANTIDAD APARECE AQUI-->
                    <span>Cantidad: </span> 
                    <select id="cantidad2" name="cantidad2">
                        <?php
                        for ($i = 1; $i <= 40; $i++) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                        ?>
                    </select> 
                    <button type="button" class="boton verde" onclick="agregarBono()" title="Agregar bonos al detalle">Agregar</button> 
                </div>
                <div class="parte-12">
                </div>
            </legend>
            <b>~*~</b><br>
        </div>  
        <div class="clearfix"></div>
        <div style="text-align: center; padding: 5px;margin: 5px;">
            <h1><i><u>Detalle de Venta</u></i></h1>
        </div>  
        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Detalle de la Venta">
            <table id="table_boleta" class="a-center" style="background-color: white">
                <thead style="text-align: center;" >
                    <tr class="headings"> 
                        <th style="text-align: center;" class="column-title">Cantidad</th>
                        <th style="text-align: center;" class="column-title">Marca</th>
                        <th style="text-align: center;" class="column-title">Descripcion</th>
                        <th style="text-align: center;" class="column-title">Precio Unitario</th>
                        <th style="text-align: center;" class="column-title">Importe</th>
                        <th style="text-align: center;" class="hidden">Precio U.</th>
                        <th style="text-align: center;" class="hidden">Importe U</th>
                        <th style="text-align: center;width: 10px;" class="column-title">Gestión</th> 
                    </tr>
                </thead>
                <tbody style="text-align: center;"  > 
                    <?php
                    $sqld = "select fd.cantidad , fd.idautos ,a.idmarca, m.descripcion marca , 
                        concat('Modelo: ',a.modelo,' / Placa: ',a.placa,' / Año: ',anio,' / Color: ',col.descripcion) automovil, a.precio precioA ,
                        fd.idbonos, bo.descripcion bono, bo.precio precioB, fd.importe
                        from fac_cabe fc 
                        inner join fac_deta fd on fc.idfac_cabe = fd.idfac_cabe
                        inner join autos a on a.idautos = fd.idautos
                        inner join marca m on m.idmarca = a.idmarca
                        inner join color col on col.idcolor = a.idcolor
                        inner join bono bo on bo.idbono = fd.idbonos
                        where fc.idfac_cabe =  $id and fd.idestado = 1 ;";
                    $resultd = mysql_query($sqld);
                    $i = 0;
                    while ($row1 = mysql_fetch_array($resultd)) {
                        $cantidad = $row1['cantidad'];
                        $idautos = $row1['idautos'];
                        $idmarca = $row1['idmarca'];
                        $marca = $row1['marca'];
                        $automovil = $row1['automovil'];
                        $precioA = $row1['precioA'];
                        $idbonos = $row1['idbonos'];
                        $bono = $row1['bono'];
                        $precioB = $row1['precioB'];
                        $importe = $row1['importe'];
                        $i++;
                        if ($idautos != 0) {
                            ?>
                            <tr id="<?php echo $i ?>" class="impreso">
                                <td style="text-align: center;"><input type="hidden" name="cantidadR<?php echo $idautos; ?>" value="<?php echo $cantidad; ?>"/><?php echo $cantidad; ?></td>
                                <td style="text-align: center;"><?php echo $marca; ?></td>
                                <td style="text-align: center;"><input type="hidden" name="idautosR<?php echo $idautos; ?>" value="<?php echo $idautos; ?>"/><?php echo $automovil; ?></td>
                                <td style="text-align: center;"><?php echo $precioA; ?></td>
                                <td style="text-align: center;"><input type="hidden" name="importeR<?php echo $idautos; ?>" value="<?php echo $importe; ?>"/><?php echo $importe; ?></td>
                                <td style="text-align: center;" class="hidden"><?php echo $precioA; ?></td>
                                <td style="text-align: center;" class="hidden"><?php echo $importe; ?></td>
                                <td style="text-align: center;"><button type="button" class="boton rojo" style="width:30px;margin:0px;padding:0px;" onclick="eliminarFila(<?php echo $i; ?>);" title="Quitar el Producto"> - </button></td>
                            </tr>
                        <?php } else { ?>
                            <tr id="<?php echo $i ?>" class="impreso">
                                <td style="text-align: center;"><input type="hidden" name="cantBonoR<?php echo $idbonos; ?>" value="<?php echo $cantidad; ?>"/><?php echo $cantidad; ?></td>
                                <td style="text-align: center;"><?php echo "Bono "; ?></td>
                                <td style="text-align: center;"><input type="hidden" name="idBonoR<?php echo $idbonos; ?>" value="<?php echo $idbonos; ?>"/><?php echo $bono; ?></td>
                                <td style="text-align: center;"><?php echo $precioB; ?></td>
                                <td style="text-align: center;"><input type="hidden" name="impoBonoR<?php echo $idbonos; ?>" value="<?php echo $importe; ?>"/><?php echo $importe; ?></td>
                                <td style="text-align: center;" class="hidden"><?php echo $precioB; ?></td>
                                <td style="text-align: center;" class="hidden"><?php echo $importe; ?></td>
                                <td style="text-align: center;"><button type="button" class="boton rojo" style="width:30px;margin:0px;padding:0px;" onclick="eliminarFila(<?php echo $i; ?>);" title="Quitar el Producto"> - </button></td>
                            </tr>

                            <?php
                        }
                    }
                    ?>

                </tbody>
                <?php if ($tipo_c == 2) { ?>
                    <tr class="a-rigth"> 
                        <td  style="background-color: white;border: none;" colspan="3"></td>
                        <td class="a-rigth"><input type="hidden" id="montoSubTotal"name="montoSubTotal"> Sub-Total $. </td>                        
                        <td class="a-center" id="subTotal">0.00</td> 
                    </tr>
                    <tr > 
                        <td  style="background-color: white;border: none;" colspan="3"></td>
                        <td class="a-rigth"><input type="hidden" id="montoIGV"name="montoIGV"> I.G.V 18 % </td>                        
                        <td class="a-center"  id="precioIgv">0.00</td> 
                    </tr>
                <?php } ?>
                <tr > 
                    <td  style="background-color: white;border: none;"  colspan="3"></td>
                    <td class="a-rigth" ><input type="hidden" id="monto_total" name="monto_total"> Total $. </td>                    
                    <td class="a-center" id="totalT">0.00</td> 
                </tr>
            </table>
            <table>
            </table>
        </div>
    </div>
    <div class="col-md-12 a-center">  
        <button type="button" id="btnRegistrar" class="boton azul" onclick="enviarForm(this)" >Modificar</button>
        <button type="button" class="boton rojo" onclick="redirectForm('listadoVentas.php')">Cancelar</button>
        <input type="hidden" name="idFacDeta" value="<?php echo $id; ?>">
        <input type="hidden" name="form" value="actualizarVenta">
    </div>
</form>
<script type="text/javascript">
    $(function () {

        var idestadoC = $('#idestadoC').val();
        if (idestadoC == 8) {
            $("#motivo").removeAttr('disabled');
            $("#motivoA").removeClass();
            $("#motivoA").addClass("box-alert");
            $("#motivo").prop('required', true);
            $("#tipo_c").prop('disabled', true);
        } else if (idestadoC == 6) {
            $("#tipo_c").prop('disabled', true);
        } else {
            $("#tipo_c").removeAttr('disabled');
        }

        $("#idautos").change(function () {
            $.post('obtenerDatos.php', {idautos: $("#idautos").val()}, function (data) {
                $("#precio").val(data);
            });
        });
        $("#idbonos").change(function () {
            $.post('obtenerDatos.php', {idbonos: $("#idbonos").val()}, function (data) {
                $("#precio2").val(data);
            });
        });
        $("#idmarca").change(function () {
            $.post('obtenerDatos.php', {idmarca: $("#idmarca").val()}, function (data) {
                document.getElementById("idautos").innerHTML = data;
            });
        });
        $("#idestadoC").change(function () {
            var idestadoC = $('#idestadoC').val();
            if (idestadoC == 8) {
                alert("Usted va a anular el Comprobante??");
                $("#motivo").val('');
                $("#motivo").removeAttr('disabled');
                $("#motivoA").removeClass();
                $("#motivoA").addClass("box-alert");
                $("#motivo").prop('required', true);
                $("#tipo_c").prop('disabled', true);
            } else if (idestadoC == 6) {
                $("#motivo").val('1');
                $("#motivo").removeAttr('required');
                $("#motivoA").removeClass();
                $("#motivoA").addClass("hidden");
                $("#motivo").prop('disabled', true);
                $("#tipo_c").prop('disabled', true);
                //document.getElementById("#motivo").value = "data";
            } else {
                $("#motivo").val('1');
                $("#motivo").removeAttr('required');
                $("#motivoA").removeClass();
                $("#motivoA").addClass("hidden");
                $("#motivo").prop('disabled', true);
                $("#tipo_c").removeAttr('disabled');
                //document.getElementById("#motivo").value = "data";
            }

        });
        calcularMonto();
    });
    function agregaAuto() {
        $("#btnRegistrar").removeClass();
        $("#btnRegistrar").addClass("boton azul");
        //capturamos los valores4
        var idmarca = $('#idmarca').val();
        var idcliente = $('#idcliente').val();
        var idauto = $('#idautos').val();
        var precio = $('#precio').val();

        var marca = $("#idmarca option:selected").html();
        var auto = $("#idautos option:selected").html();
        var cantidadN = $("#cantidad option:selected").html();

        var cat = document.getElementById("table_boleta").rows.length;

        var importe = (cantidadN * precio);

        if (idcliente == 0) {
            alert("Seleccione un Cliente");
            $('#idcliente').focus();
        } else if (idauto == 0) {
            alert("Seleccione una Auto");
            $('#idautos').focus();
        } else if (idmarca == 99) {
            alert("Seleccione una marca");
            $('#idmarca').focus();
        } else {
            if (idmarca > 0 && idauto > 0) {
                $('#table_boleta').append(
                        '<tr id="' + cat + '" class="impreso">\n\
                <td style="text-align: center;"><input type="hidden" name="cantidadR' + idauto + '" value="' + cantidadN + '"/>' + cantidadN + '</td>\n\
                <td style="text-align: center;">' + marca + '</td>\n\
                <td style="text-align: center;"><input type="hidden" name="idautosR' + idauto + '" value="' + idauto + '"/>' + auto + '</td>\n\
                <td style="text-align: center;">' + precio + '</td>\n\
                <td style="text-align: center;"><input type="hidden" name="importeR' + idauto + '" value="' + importe.toFixed(2) + '"/>' + importe.toFixed(2) + '</td>\n\
                <td style="text-align: center;" class="hidden">' + precio + '</td>\n\
                <td style="text-align: center;" class="hidden">' + importe + '</td>\n\
                <td style="text-align: center;"><button type="button" class="boton rojo" style="width:30px;margin:0px;padding:0px;" onclick="eliminarFila(' + cat + ');" title="Quitar el Producto"><i class="fa fa-minus-square"></i> - </button></td>\n\
            </tr>\n\
            ');
            }
            calcularMonto();
            //limiamos los textbox
            $('#idmarca').val("");
            $('#idautos').val("");
            $('#cantidad').val("");
            $('#precio').val("");
            ocuBusAuto();
        }
    }
    function habBusAuto() { //btnMostrarAutos
        $("#btnMostrarAutos").removeClass();
        $("#btnMostrarAutos").addClass("hidden");
        $("#habilitarAutos").removeClass();
        $("#habilitarAutos").addClass("box-alert");
    }
    function ocuBusAuto() { //btnMostrarAutos
        $("#habilitarAutos").removeClass();
        $("#habilitarAutos").addClass("hidden");
        $("#btnMostrarAutos").removeClass();
        $("#btnMostrarAutos").addClass("boton verde");
    }

    function habBusBono() { //btnMostrarAutos
        $("#btnMostrarBonos").removeClass();
        $("#btnMostrarBonos").addClass("hidden");
        $("#habilitarBonos").removeClass();
        $("#habilitarBonos").addClass("box-alert");
    }
    function ocuBusBono() { //btnMostrarAutos
        $("#habilitarBonos").removeClass();
        $("#habilitarBonos").addClass("hidden");
        $("#btnMostrarBonos").removeClass();
        $("#btnMostrarBonos").addClass("boton verde");
    }

    function agregarBono() {
        //capturamos los valores
        var idbonos = $('#idbonos').val();
        var precio = $('#precio2').val();

        var bono = $("#idbonos option:selected").html();
        var cantidadN2 = $("#cantidad2 option:selected").html();

        var cat = document.getElementById("table_boleta").rows.length;

        var importe = (cantidadN2 * precio);

        if (idbonos == 0) {
            alert("Seleccione un Bono");
            $('#idbonos').focus();
        } else {
            if (idbonos > 0) {
                $('#table_boleta').append(
                        '<tr id="' + cat + '" class="impreso">\n\
                <td style="text-align: center;"><input type="hidden" name="cantBonoR' + idbonos + '" value="' + cantidadN2 + '"/>' + cantidadN2 + '</td>\n\
                <td style="text-align: center;">Bono</td>\n\
                <td style="text-align: center;"><input type="hidden" name="idBonoR' + idbonos + '" value="' + idbonos + '"/>' + bono + '</td>\n\
                <td style="text-align: center;">' + precio + '</td>\n\
                <td style="text-align: center;"><input type="hidden" name="impoBonoR' + idbonos + '" value="' + importe.toFixed(2) + '"/>' + importe.toFixed(2) + '</td>\n\
                <td style="text-align: center;" class="hidden">' + precio + '</td>\n\
                <td style="text-align: center;" class="hidden">' + importe + '</td>\n\
                <td style="text-align: center;"><button type="button" class="boton rojo" style="width:30px;margin:0px;padding:0px;" onclick="eliminarFila(' + cat + ');" title="Quitar el Producto"><i class="fa fa-minus-square"></i> - </button></td>\n\
            </tr>\n\
            ');
            }
            calcularMonto();
            //limiamos los textbox
            $('#idbonos').val("");
            $('#precio2').val("");
            $('#cantidad').val("");
            ocuBusBono();
        }
    }
    function calcularMonto() {
        var total2 = 0.00;
        $("#table_boleta tr").find('td:eq(6)').each(function () {
            var valor1 = $(this).html();
            total2 += parseFloat(valor1);
        });
<?php
if ($tipo_c == 2) {
    ?>
            var subtotal, igv, total = 0.00;
            subtotal = total2;
            igv = subtotal * parseFloat(0.18);
            total = subtotal + igv;
            $('#subTotal').text(subtotal.toFixed(2));
            $('#precioIgv').text(igv.toFixed(2));
            $('#totalT').text(total.toFixed(2));

            document.getElementById("montoSubTotal").value = subtotal.toFixed(2);
            document.getElementById("montoIGV").value = igv.toFixed(2);
            document.getElementById("monto_total").value = total.toFixed(2);
<?php } else { ?>
            $('#totalT').text(total2.toFixed(2));
            document.getElementById("monto_total").value = total2.toFixed(2);
<?php } ?>
    }
    function eliminarFila(id)
    {
        $('#' + id).remove();
        calcularMonto();
    }
</script>
<?php
include 'footer.php';
