-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lyonneBD
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anulaciones`
--

DROP TABLE IF EXISTS `anulaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anulaciones` (
  `idanulaciones` int(11) NOT NULL AUTO_INCREMENT,
  `idfac_cabe` int(11) NOT NULL,
  `idcolaborador` int(11) NOT NULL,
  `fecha_anulacion` datetime NOT NULL,
  `motivo` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idanulaciones`),
  KEY `fk_anulaciones_1idfac_cabe_idx` (`idfac_cabe`),
  KEY `fk_anulaciones_1idcolaborador_idx` (`idcolaborador`),
  KEY `fk_anulaciones_1idestado_idx` (`idestado`),
  CONSTRAINT `fk_anulaciones_1idcolaborador` FOREIGN KEY (`idcolaborador`) REFERENCES `colaborador` (`idcolaborador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anulaciones_1idestado` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anulaciones_1idfac_cabe` FOREIGN KEY (`idfac_cabe`) REFERENCES `fac_cabe` (`idfac_cabe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anulaciones`
--

LOCK TABLES `anulaciones` WRITE;
/*!40000 ALTER TABLE `anulaciones` DISABLE KEYS */;
INSERT INTO `anulaciones` VALUES (1,23,1,'2017-06-21 18:32:48','Se generÃ³ un error al registrar la boleta a nombre de otro cliente',1,'2017-06-21 23:32:48'),(2,27,1,'2017-06-21 18:34:02','se registro la boleta sin productos',1,'2017-06-21 23:34:02'),(3,32,1,'2017-06-22 16:56:19','1                    ',1,'2017-06-22 21:56:19');
/*!40000 ALTER TABLE `anulaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autos`
--

DROP TABLE IF EXISTS `autos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autos` (
  `idautos` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `precio` double(10,2) NOT NULL,
  `placa` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anio` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `idmarca` int(11) NOT NULL,
  `idcolor` int(11) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idautos`),
  UNIQUE KEY `placa_UNIQUE` (`placa`),
  KEY `fk_autos_marca1_idx` (`idmarca`),
  KEY `fk_autos_color1_idx` (`idcolor`),
  KEY `fk_autos_estado1_idx` (`idestado`),
  CONSTRAINT `fk_autos_color1` FOREIGN KEY (`idcolor`) REFERENCES `color` (`idcolor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_autos_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_autos_marca1` FOREIGN KEY (`idmarca`) REFERENCES `marca` (`idmarca`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=628 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autos`
--

LOCK TABLES `autos` WRITE;
/*!40000 ALTER TABLE `autos` DISABLE KEYS */;
INSERT INTO `autos` VALUES (0,'Bono',0.00,'Bono','Bono','2000',1,1,2,'2017-06-21 07:41:00'),(466,'Mercedes S65 AMG',47000.00,'PER-4500I','','2012',1,1,1,'2017-06-19 22:59:51'),(467,'Mercedes-Benz Clase SL',32000.00,'PER-M19S','','2013',1,2,1,'2017-06-19 22:59:51'),(468,'Mercedes-Benz MB 100',37500.00,'PER-451S1','','2012',1,3,1,'2017-06-19 22:59:51'),(469,'Mercedes-Benz Necar 5',43200.00,'PER-74D4S','','2017',1,4,1,'2017-06-19 22:59:51'),(470,'Mercedes S65 AMG',47000.00,'PER-4740I','','2012',1,1,1,'2017-06-19 22:38:02'),(471,'Serie 1 (3 puertas)',67729.00,'PER-6073','','2013',2,2,1,'2017-06-19 22:38:02'),(472,'Serie 2 Coup',238974.00,'PER-1862','','2013',2,7,1,'2017-06-20 01:35:14'),(473,'M140i (5 puertas)',58228.00,'PER-1669','','2014',2,1,1,'2017-06-19 22:38:02'),(474,'M240i Convertible',120089.00,'PER-3936','','2014',2,7,1,'2017-06-19 22:38:02'),(475,'7108 Boxter',95586.00,'PER-6667','','2013',3,3,1,'2017-06-19 22:38:02'),(476,'Cayenne',114846.00,'PER-5221','','2015',3,9,1,'2017-06-19 22:38:02'),(477,'Macan',71618.00,'PER-5509','','2014',3,1,1,'2017-06-19 22:38:02'),(478,'Panamera',48599.00,'PER-5056','','2013',3,7,1,'2017-06-19 22:38:02'),(479,'P-911',151586.00,'PER-5747','','2013',3,10,1,'2017-06-19 22:38:02'),(480,'7108 Cayman',166401.00,'PER-4036','','2013',3,9,1,'2017-06-19 22:38:02'),(481,'911 GT2 RS',146761.00,'PER-4862','','2013',3,4,1,'2017-06-19 22:38:02'),(482,'911 GT3 Facelift',78353.00,'PER-6135','','2014',3,2,1,'2017-06-19 22:38:02'),(483,'ALL-NEW PICANTO',105210.00,'PER-9075','','2013',4,5,1,'2017-06-19 22:38:02'),(484,'RIO SEDAN',69663.00,'PER-5718','','2013',4,2,1,'2017-06-19 22:38:02'),(485,'CERATO COUP',183174.00,'PER-9391','','2015',4,7,1,'2017-06-19 22:38:02'),(486,'SORENTO',170989.00,'PER-9558','','2015',4,8,1,'2017-06-19 22:38:02'),(487,'OPTIMA',82252.00,'PER-3756','','2013',4,8,1,'2017-06-19 22:38:02'),(488,'CANDEZA',133836.00,'PER-9872','','2014',4,6,1,'2017-06-19 22:38:02'),(489,'SOUL',201761.00,'PER-5625','','2013',4,7,1,'2017-06-19 22:38:02'),(490,'CERATO SEDAN',42000.00,'PER-5634','','2014',4,10,1,'2017-06-19 22:38:02'),(491,'HR-V',162800.00,'PER-8599','','2013',5,9,1,'2017-06-19 22:38:02'),(492,'GOLDWING',242146.00,'PER-8515','','2014',5,7,1,'2017-06-19 22:38:02'),(493,'INSIGHT',174343.00,'PER-2724','','2015',5,4,1,'2017-06-19 22:38:02'),(494,'INTEGRA',223873.00,'PER-2807','','2013',5,9,1,'2017-06-19 22:38:02'),(495,'RSX',127900.00,'PER-9723','','2013',5,7,1,'2017-06-19 22:38:02'),(496,'FR-V',63943.00,'PER-1881','','2014',5,2,1,'2017-06-19 22:38:02'),(497,'CR-Z',163961.00,'PER-8390','','2013',5,7,1,'2017-06-19 22:38:02'),(498,'CIVIC TYPE-R',35714.00,'PER-5262','','2014',5,6,1,'2017-06-19 22:38:02'),(499,'GRANVIA',175938.00,'PER-5146','','2014',6,6,1,'2017-06-19 22:38:02'),(500,'URBAN CRUISER',69645.00,'PER-8569','','2013',6,4,1,'2017-06-19 22:38:02'),(501,'LAND CRUISER',137720.00,'PER-3985','','2013',6,8,1,'2017-06-19 22:38:02'),(502,'MATRIX',185374.00,'PER-1423','','2014',6,2,1,'2017-06-19 22:38:02'),(503,'PASEO',111726.00,'PER-9925','','2013',6,5,1,'2017-06-19 22:38:02'),(504,'PLATZ',75604.00,'PER-3409','','2013',6,1,1,'2017-06-19 22:38:02'),(505,'PRIUS',244739.00,'PER-9365','','2013',6,2,1,'2017-06-19 22:38:02'),(506,'HARRIER',217260.00,'PER-81753','','2013',6,8,1,'2017-06-19 22:38:02'),(507,'CHIRON',232521.00,'PER-4218','','2015',7,4,1,'2017-06-19 22:38:02'),(508,'TYPE 55',197585.00,'PER-3486','','2014',7,3,1,'2017-06-19 22:38:02'),(509,'TYPE 57',171845.00,'PER-3768','','2015',7,6,1,'2017-06-19 22:38:02'),(510,'EB110',55770.00,'PER-9942','','2013',7,4,1,'2017-06-19 22:38:02'),(511,'REB118',54705.00,'PER-7637','','2015',7,5,1,'2017-06-19 22:38:02'),(512,'16C GALIBIER',227706.00,'PER-2533','','2015',7,5,1,'2017-06-19 22:38:02'),(513,'ROYALE',137741.00,'PER-8714','','2015',7,5,1,'2017-06-19 22:38:02'),(514,'STRATOS',220328.00,'PER-2402','','2014',7,3,1,'2017-06-19 22:38:02'),(515,'AMAROK',224157.00,'PER-9498','','2015',8,6,1,'2017-06-19 22:38:02'),(516,'CC',112402.00,'PER-9003','','2014',8,9,1,'2017-06-19 22:38:02'),(517,'CROSSFOX',80391.00,'PER-5914','','2014',8,10,1,'2017-06-19 22:38:02'),(518,'GOL',47810.00,'PER-7902','','2013',8,6,1,'2017-06-19 22:38:02'),(519,'GOL SEDAN',111653.00,'PER-2609','','2015',8,5,1,'2017-06-19 22:38:02'),(520,'JETTA',50536.00,'PER-9301','','2015',8,3,1,'2017-06-19 22:38:02'),(521,'POLO',196122.00,'PER-5119','','2015',8,10,1,'2017-06-19 22:38:02'),(522,'TIGUAN',51878.00,'PER-1644','','2014',8,7,1,'2017-06-19 22:38:02'),(523,'C-MAX ENERGI',192658.00,'PER-3701','','2013',9,7,1,'2017-06-19 22:38:02'),(524,'COURIER',64723.00,'PER-5650','','2014',9,6,1,'2017-06-19 22:38:02'),(525,'EDGE',71458.00,'PER-2161','','2014',9,2,1,'2017-06-19 22:38:02'),(526,'ESCAPE',185226.00,'PER-9624','','2015',9,7,1,'2017-06-19 22:38:02'),(527,'GT40',111843.00,'PER-8665','','2015',9,7,1,'2017-06-19 22:38:02'),(528,'FESTIVA',160617.00,'PER-4675','','2014',9,6,1,'2017-06-19 22:38:02'),(529,'KUGA',186336.00,'PER-3788','','2015',9,3,1,'2017-06-19 22:38:02'),(530,'FAIRMONT',162109.00,'PER-8194','','2014',9,2,1,'2017-06-19 22:38:02'),(531,'BORA',95368.00,'PER-7296','','2014',10,5,1,'2017-06-19 22:38:02'),(532,'MISTRAL',62238.00,'PER-3144','','2015',10,8,1,'2017-06-19 22:38:02'),(533,'KYALAMI',209780.00,'PER-3912','','2014',10,5,1,'2017-06-19 22:38:02'),(534,'KHAMSIN',103700.00,'PER-9908','','2013',10,6,1,'2017-06-19 22:38:02'),(535,'MERAK',156443.00,'PER-5592','','2015',10,2,1,'2017-06-19 22:38:02'),(536,'5000 GT',152391.00,'PER-9339','','2015',10,1,1,'2017-06-19 22:38:02'),(537,'RACING',190353.00,'PER-6796','','2013',10,8,1,'2017-06-19 22:38:02'),(538,'SHAMAL',100978.00,'PER-3240','','2014',10,5,1,'2017-06-19 22:38:02'),(539,'CUBE',131582.00,'PER-6501','','2014',11,6,1,'2017-06-19 22:38:02'),(540,'ESFLOW',92531.00,'PER-9258','','2015',11,6,1,'2017-06-19 22:38:02'),(541,'280 ZX',227315.00,'PER-9757','','2015',11,2,1,'2017-06-19 22:38:02'),(542,'JUKE',61447.00,'PER-9390','','2014',11,1,1,'2017-06-19 22:38:02'),(543,'GT-R',159653.00,'PER-2931','','2014',11,7,1,'2017-06-19 22:38:02'),(544,'MURANO',107277.00,'PER-3004','','2013',11,10,1,'2017-06-19 22:38:02'),(545,'ALMERA',249297.00,'PER-7101','','2015',11,3,1,'2017-06-19 22:38:02'),(546,'NV200',160427.00,'PER-7619','','2013',11,1,1,'2017-06-19 22:38:02'),(547,'IMPALA',77099.00,'PER-6765','','2013',12,2,2,'2017-06-22 18:40:23'),(548,'SERIE N',163841.00,'PER-5486','','2014',12,8,1,'2017-06-19 22:38:02'),(549,'KADETT',172447.00,'PER-8549','','2015',12,8,1,'2017-06-19 22:38:02'),(550,'ISUZU D-MAX',108949.00,'PER-4552','','2015',12,7,1,'2017-06-19 22:38:02'),(551,'LUMINA',225033.00,'PER-5501','','2014',12,1,1,'2017-06-19 22:38:02'),(552,'MALIBU',189324.00,'PER-9966','','2013',12,2,1,'2017-06-19 22:38:02'),(553,'CALIBRA',70179.00,'PER-4161','','2015',12,4,1,'2017-06-19 22:38:02'),(554,'CELEBRITY',200478.00,'PER-2693','','2015',12,5,1,'2017-06-19 22:38:02'),(555,'V-400',99867.00,'PER-3958','','2014',13,2,1,'2017-06-19 22:38:02'),(556,'V-440',216563.00,'PER-5693','','2014',13,3,1,'2017-06-19 22:38:02'),(557,'V-680',226291.00,'PER-1341','','2015',13,7,1,'2017-06-19 22:38:02'),(558,'SERIE 100',106275.00,'PER-6026','','2013',13,2,1,'2017-06-19 22:38:02'),(559,'V-140',247442.00,'PER-6927','','2013',13,3,1,'2017-06-19 22:38:02'),(560,'V-850',53931.00,'PER-6810','','2015',13,3,1,'2017-06-19 22:38:02'),(561,'SERIE 900',74328.00,'PER-3288','','2015',13,4,1,'2017-06-19 22:38:02'),(562,'V-ECC',129440.00,'PER-2937','','2014',13,5,1,'2017-06-19 22:38:02'),(563,'M-6',80550.00,'PER-1334','','2015',14,4,1,'2017-06-19 22:38:02'),(564,'M-3',186067.00,'PER-4197','','2014',14,6,1,'2017-06-19 22:38:02'),(565,'M-323',112115.00,'PER-4591','','2015',14,2,1,'2017-06-19 22:38:02'),(566,'M-787B',204399.00,'PER-2456','','2015',14,1,1,'2017-06-19 22:38:02'),(567,'ATENZA',234423.00,'PER-3966','','2015',14,1,1,'2017-06-19 22:38:02'),(568,'AXELA',121914.00,'PER-1979','','2013',14,4,1,'2017-06-19 22:38:02'),(569,'M-BT-50',180941.00,'PER-6310','','2013',14,4,1,'2017-06-19 22:38:02'),(570,'CX-7',231512.00,'PER-3202','','2015',14,2,1,'2017-06-19 22:38:02'),(571,'CARISMA',43136.00,'PER-2859','','2015',15,2,1,'2017-06-19 22:38:02'),(572,'CHALLENGER',238201.00,'PER-6562','','2015',15,8,1,'2017-06-19 22:38:02'),(573,'COLT',205486.00,'PER-9675','','2013',15,2,1,'2017-06-19 22:38:02'),(574,'ECLIPSE',53115.00,'PER-1619','','2013',15,10,1,'2017-06-19 22:38:02'),(575,'ENDEAVOR',71311.00,'PER-6978','','2014',15,1,1,'2017-06-19 22:38:02'),(576,'L200',107539.00,'PER-6513','','2013',15,4,1,'2017-06-19 22:38:02'),(577,'LANCER',185968.00,'PER-7695','','2013',15,7,1,'2017-06-19 22:38:02'),(578,'GALANT',129102.00,'PER-8071','','2015',15,9,1,'2017-06-19 22:38:02'),(579,'R-10',169288.00,'PER-3318','','2013',16,5,1,'2017-06-19 22:38:02'),(580,'R-20/30',157386.00,'PER-3599','','2013',16,10,1,'2017-06-19 22:38:02'),(581,'R-4CV',146740.00,'PER-8940','','2013',16,3,1,'2017-06-19 22:38:02'),(582,'R-9/11',176216.00,'PER-2505','','2014',16,3,1,'2017-06-19 22:38:02'),(583,'AVANTINE',125936.00,'PER-3191','','2013',16,9,1,'2017-06-19 22:38:02'),(584,'BE BOP',130157.00,'PER-9140','','2015',16,5,1,'2017-06-19 22:38:02'),(585,'CARAVELLE',145812.00,'PER-1036','','2013',16,8,1,'2017-06-19 22:38:02'),(586,'DAUPHINE',95794.00,'PER-2329','','2015',16,4,1,'2017-06-19 22:38:02'),(587,'CITYCAR',181720.00,'PER-2995','','2014',17,9,1,'2017-06-19 22:38:02'),(588,'BOXER',203934.00,'PER-8753','','2013',17,3,1,'2017-06-19 22:38:02'),(589,'J5',167016.00,'PER-1593','','2013',17,9,1,'2017-06-19 22:38:02'),(590,'P4',160023.00,'PER-7551','','2013',17,5,1,'2017-06-19 22:38:02'),(591,'TIPO 105',65492.00,'PER-6458','','2013',17,8,1,'2017-06-19 22:38:02'),(592,'MITSUBISHI RVR',177782.00,'PER-3816','','2015',17,8,1,'2017-06-19 22:38:02'),(593,'OXIA',247764.00,'PER-9182','','2014',17,3,1,'2017-06-19 22:38:02'),(594,'RCZ',44951.00,'PER-3991','','2015',17,6,1,'2017-06-19 22:38:02'),(595,'ACCENT',222746.00,'PER-3963','','2014',18,3,1,'2017-06-19 22:38:02'),(596,'ATOS',176132.00,'PER-3589','','2014',18,9,1,'2017-06-19 22:38:02'),(597,'AZERA',155787.00,'PER-2193','','2015',18,7,1,'2017-06-19 22:38:02'),(598,'CLICK',232467.00,'PER-3634','','2015',18,1,1,'2017-06-19 22:38:02'),(599,'COUPE',119851.00,'PER-2876','','2014',18,7,1,'2017-06-19 22:38:02'),(600,'ELANTRA',205611.00,'PER-9490','','2015',18,3,1,'2017-06-19 22:38:02'),(601,'GENESIS',189490.00,'PER-2640','','2014',18,3,1,'2017-06-19 22:38:02'),(602,'GETZ',181296.00,'PER-7628','','2015',18,10,1,'2017-06-19 22:38:02'),(603,'BRAVO',118764.00,'PER-3109','','2014',19,9,1,'2017-06-19 22:38:02'),(604,'ATHON',35623.00,'PER-3145','','2014',19,6,1,'2017-06-19 22:38:02'),(605,'AVENTADOR',41538.00,'PER-6592','','2014',19,6,1,'2017-06-19 22:38:02'),(606,'DIABLO',41487.00,'PER-9585','','2015',19,1,1,'2017-06-19 22:38:02'),(607,'ISLERO',209621.00,'PER-62135','','2013',19,2,1,'2017-06-19 22:38:02'),(608,'HURACAN',172969.00,'PER-1694','','2013',19,2,1,'2017-06-19 22:38:02'),(609,'GALLARDO',99485.00,'PER-8444','','2014',19,1,1,'2017-06-19 22:38:02'),(610,'VENENO',92165.00,'PER-3190','','2015',19,9,1,'2017-06-19 22:38:02'),(611,'250 GT BERLINETTA',112239.00,'PER-2457','','2014',20,6,1,'2017-06-19 22:38:02'),(612,'612 SCAGLIETTI',77397.00,'PER-9876','','2015',20,7,1,'2017-06-19 22:38:02'),(613,'458 ITALIA',131658.00,'PER-3576','','2015',20,9,1,'2017-06-19 22:38:02'),(614,'575M MARANELLO',73474.00,'PER-4051','','2013',20,5,1,'2017-06-19 22:38:02'),(615,'F12BERLINETTA',203562.00,'PER-3950','','2015',20,7,1,'2017-06-19 22:38:02'),(616,'CALIFORNIA',51831.00,'PER-2587','','2015',20,7,1,'2017-06-19 22:38:02'),(617,'ENZO',187061.00,'PER-8619','','2015',20,4,1,'2017-06-19 22:38:02'),(618,'DAYTONA',60061.00,'PER-3392','','2014',20,6,1,'2017-06-19 22:38:02'),(619,'ALLROAD QUATTRO CONCEPT',209969.00,'PER-2777','','2013',21,10,1,'2017-06-19 22:38:02'),(620,'CROSS CABRIOLET',54200.00,'PER-7616','','2013',21,9,1,'2017-06-19 23:57:28'),(621,'QUATTRO CONCEP',60559.00,'PER-8826','','2013',21,6,2,'2017-06-22 18:09:33'),(622,'ROADJET',126099.00,'PER-2915','','2015',21,6,1,'2017-06-19 22:38:02'),(623,'LE MAS QUATTRO',87894.00,'PER-9923','','2014',21,2,1,'2017-06-19 22:38:02'),(624,'NUVOLARI',103876.00,'PER-7979','','2014',21,10,1,'2017-06-19 22:38:02'),(625,'SILHOUETTE VARGAS',34150.75,'PER-CS454','','2013',19,2,1,'2017-06-20 01:47:35'),(627,'NEW ROAD CHEV',45000.00,'PER-CS45','','2017',21,7,1,'2017-06-21 08:19:45');
/*!40000 ALTER TABLE `autos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bono`
--

DROP TABLE IF EXISTS `bono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bono` (
  `idbono` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `precio` double(10,2) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbono`),
  KEY `fk_bono_estado1_idx` (`idestado`),
  CONSTRAINT `fk_bono_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bono`
--

LOCK TABLES `bono` WRITE;
/*!40000 ALTER TABLE `bono` DISABLE KEYS */;
INSERT INTO `bono` VALUES (0,'Auto',0.00,2,'2017-06-21 07:41:48'),(1,'Revision Mensual',88.99,1,'2017-06-22 00:13:28'),(2,'Lavado Gratis',0.00,1,'2017-06-21 07:03:33'),(3,'Revision Tecnica',45.50,1,'2017-06-22 00:12:01'),(4,'SOAT',252.98,1,'2017-06-22 00:12:25'),(5,'Aromatizantes de auto',4.53,2,'2017-06-22 18:22:11');
/*!40000 ALTER TABLE `bono` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idestado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcargo`),
  KEY `fk_cargo_estado1_idx` (`idestado`),
  CONSTRAINT `fk_cargo_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'Gerente','2017-06-19 07:58:07',1),(2,'Administrador','2017-06-19 07:58:07',1),(3,'Vendedor','2017-06-19 07:58:07',1);
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `RUC` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idpersona` int(11) NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcliente`),
  KEY `fk_cliente_persona1_idx` (`idpersona`),
  CONSTRAINT `fk_cliente_persona1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'',2,'2017-06-19 16:08:41'),(2,'10007412452',3,'2017-06-19 18:09:29'),(3,'',7,'2017-06-21 08:23:00'),(4,'10741542512',8,'2017-06-21 09:48:35'),(5,'10412745782',9,'2017-06-23 01:52:34'),(6,'104127458962',10,'2017-06-23 02:19:35');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colaborador`
--

DROP TABLE IF EXISTS `colaborador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colaborador` (
  `idcolaborador` int(11) NOT NULL AUTO_INCREMENT,
  `idpersona` int(11) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `fecha_desceso` date NOT NULL DEFAULT '0000-00-00',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idcargo` int(11) NOT NULL,
  PRIMARY KEY (`idcolaborador`),
  KEY `fk_colaborador_persona1_idx` (`idpersona`),
  KEY `fk_colaborador_cargo1_idx` (`idcargo`),
  CONSTRAINT `fk_colaborador_cargo1` FOREIGN KEY (`idcargo`) REFERENCES `cargo` (`idcargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_colaborador_persona1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colaborador`
--

LOCK TABLES `colaborador` WRITE;
/*!40000 ALTER TABLE `colaborador` DISABLE KEYS */;
INSERT INTO `colaborador` VALUES (1,1,'2015-04-02','0000-00-00','2017-06-19 21:39:47',2),(2,4,'2017-06-19','0000-00-00','2017-06-20 00:48:44',2),(3,5,'2017-06-19','0000-00-00','2017-06-20 00:53:52',3),(4,6,'2017-06-19','0000-00-00','2017-06-20 00:58:47',3);
/*!40000 ALTER TABLE `colaborador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `idcolor` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idestado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcolor`),
  KEY `fk_color_estado1_idx` (`idestado`),
  CONSTRAINT `fk_color_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'Rojo','2017-06-19 07:59:44',1),(2,'Blanco','2017-06-19 07:59:44',1),(3,'Negro','2017-06-19 07:59:44',1),(4,'Azul','2017-06-19 07:59:44',1),(5,'Verde Oscuro','2017-06-19 07:59:44',1),(6,'Plomo','2017-06-19 07:59:44',1),(7,'Acero','2017-06-19 07:59:44',1),(8,'Crema','2017-06-19 07:59:44',1),(9,'Arena','2017-06-19 07:59:44',1),(10,'Anaranjado','2017-06-19 07:59:44',1);
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Habilitado','2017-06-19 07:56:17'),(2,'Deshabilitado','2017-06-19 07:56:17'),(3,'Offline','2017-06-19 07:56:17'),(4,'Online','2017-06-19 07:56:17'),(5,'Pendiente de pago','2017-06-19 07:56:17'),(6,'Pagado','2017-06-19 07:56:17'),(7,'Eliminado','2017-06-19 07:56:17'),(8,'Anulado','2017-06-21 09:06:27');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fac_cabe`
--

DROP TABLE IF EXISTS `fac_cabe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fac_cabe` (
  `idfac_cabe` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_emision` datetime NOT NULL,
  `idcliente` int(11) NOT NULL,
  `total` double(10,2) NOT NULL,
  `idcolaborador` int(11) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '5',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_c` int(11) NOT NULL DEFAULT '1',
  `monto_igv` double(10,2) NOT NULL DEFAULT '0.00',
  `monto_subtotal` double(10,2) NOT NULL DEFAULT '0.00',
  `numero_comp` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idfac_cabe`),
  KEY `fk_fac_cabe_cliente1_idx` (`idcliente`),
  KEY `fk_fac_cabe_colaborador1_idx` (`idcolaborador`),
  KEY `fk_fac_cabe_estado1_idx` (`idestado`),
  KEY `fk_fac_cabe_tipo_c1_idx` (`tipo_c`),
  CONSTRAINT `fk_fac_cabe_cliente1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_colaborador1` FOREIGN KEY (`idcolaborador`) REFERENCES `colaborador` (`idcolaborador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_tipo_c1` FOREIGN KEY (`tipo_c`) REFERENCES `tipo_comprobante` (`idtipo_c`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fac_cabe`
--

LOCK TABLES `fac_cabe` WRITE;
/*!40000 ALTER TABLE `fac_cabe` DISABLE KEYS */;
INSERT INTO `fac_cabe` VALUES (1,'2017-06-21 02:47:35',2,314105.50,3,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0001'),(4,'2017-06-21 03:12:43',2,558789.50,2,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0002'),(5,'2017-06-21 03:17:33',1,183064.49,3,6,'2017-06-22 20:10:12',2,27925.09,155139.40,'F0001'),(6,'2017-06-21 03:23:52',3,363910.70,4,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0003'),(12,'2017-06-21 03:30:40',1,209969.00,2,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0004'),(13,'2017-06-21 03:34:35',1,172447.00,2,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0005'),(14,'2017-06-21 03:35:51',1,197585.00,2,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0006'),(15,'2017-06-21 03:56:06',2,342004.12,4,6,'2017-06-22 20:10:12',2,52170.12,289834.00,'F0002'),(16,'2017-06-21 03:56:27',3,323615.00,3,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0007'),(17,'2017-06-21 03:56:46',2,79920.22,2,6,'2017-06-22 20:10:12',2,12191.22,67729.00,'F0003'),(18,'2017-06-21 03:57:07',3,137418.08,4,6,'2017-06-22 20:10:12',2,20962.08,116456.00,'F0004'),(19,'2017-06-21 03:57:23',3,53100.00,2,6,'2017-06-22 20:10:12',2,8100.00,45000.00,'F0005'),(20,'2017-06-21 04:17:29',2,197585.00,3,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0008'),(21,'2017-06-21 04:20:11',2,126099.00,1,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0009'),(22,'2017-06-21 04:23:14',3,132442.02,4,6,'2017-06-22 20:10:12',2,20203.02,112239.00,'F0006'),(23,'2017-06-21 04:49:09',1,0.00,1,8,'2017-06-22 20:09:33',1,0.00,0.00,'B0010'),(24,'2017-06-21 04:56:32',4,55770.00,1,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0011'),(25,'2017-06-21 04:57:54',1,49570.27,1,6,'2017-06-22 20:10:12',2,7561.57,42008.70,'F0007'),(26,'2017-06-21 05:10:00',4,100536.00,1,6,'2017-06-22 20:10:12',2,15336.00,85200.00,'F0008'),(27,'2017-06-21 05:12:31',1,0.00,1,8,'2017-06-22 20:09:33',1,0.00,0.00,'B0012'),(28,'2017-06-21 18:27:39',4,1208508.80,1,6,'2017-06-22 20:10:12',2,184348.80,1024160.00,'F0009'),(29,'2017-06-21 19:14:36',2,517093.99,1,6,'2017-06-22 20:09:33',1,0.00,0.00,'B0013'),(30,'2017-06-22 13:54:35',3,590206.91,1,6,'2017-06-23 01:46:17',1,0.00,0.00,'B0014'),(31,'2017-06-22 15:32:34',1,129032.98,1,6,'2017-06-23 01:34:29',2,19683.00,109349.98,'F0010'),(32,'2017-06-22 16:52:07',1,1014139.20,1,6,'2017-06-23 02:11:16',2,154699.20,859440.00,'F0012'),(33,'2017-06-22 21:07:27',5,762703.62,1,6,'2017-06-23 02:10:07',2,116344.62,646359.00,'F0011');
/*!40000 ALTER TABLE `fac_cabe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fac_deta`
--

DROP TABLE IF EXISTS `fac_deta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fac_deta` (
  `idfac_cabe` int(11) NOT NULL,
  `idautos` int(11) NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL,
  `importe` double(10,2) NOT NULL,
  `descuento` double(10,2) NOT NULL DEFAULT '0.00',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idbonos` int(11) NOT NULL DEFAULT '0',
  `idestado` int(11) NOT NULL DEFAULT '1',
  KEY `fk_fac_cabe_has_autos_autos1_idx` (`idautos`),
  KEY `fk_fac_cabe_has_autos_fac_cabe1_idx` (`idfac_cabe`),
  KEY `fk_fac_cabe_has_autos_bonos1_idx` (`idbonos`),
  KEY `fk_fac_deta_idestado1_idx` (`idestado`),
  CONSTRAINT `fk_fac_cabe_has_autos_autos1` FOREIGN KEY (`idautos`) REFERENCES `autos` (`idautos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_has_autos_bonos1` FOREIGN KEY (`idbonos`) REFERENCES `bono` (`idbono`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_has_autos_fac_cabe1` FOREIGN KEY (`idfac_cabe`) REFERENCES `fac_cabe` (`idfac_cabe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_deta_11234213idestado` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fac_deta`
--

LOCK TABLES `fac_deta` WRITE;
/*!40000 ALTER TABLE `fac_deta` DISABLE KEYS */;
INSERT INTO `fac_deta` VALUES (1,466,3,141000.00,0.00,'2017-06-21 12:47:29',0,1),(1,608,1,172969.00,0.00,'2017-06-21 12:47:29',0,1),(1,0,1,0.00,0.00,'2017-06-21 12:47:29',2,1),(1,0,3,136.50,0.00,'2017-06-21 12:47:29',3,1),(4,468,1,37500.00,0.00,'2017-06-21 12:47:29',0,1),(4,526,1,185226.00,0.00,'2017-06-21 12:47:29',0,1),(4,473,1,58228.00,0.00,'2017-06-21 12:47:29',0,1),(4,513,2,275482.00,0.00,'2017-06-21 12:47:29',0,1),(4,0,5,0.00,0.00,'2017-06-21 12:47:29',2,1),(4,0,5,2126.00,0.00,'2017-06-21 12:47:29',4,1),(4,0,5,227.50,0.00,'2017-06-21 12:47:29',3,1),(5,547,2,154198.00,0.00,'2017-06-21 12:47:29',0,1),(5,0,2,0.00,0.00,'2017-06-21 12:47:29',2,1),(5,0,2,91.00,0.00,'2017-06-21 12:47:29',3,1),(5,0,2,850.40,0.00,'2017-06-21 12:47:29',4,1),(6,587,2,363440.00,0.00,'2017-06-21 12:47:29',0,1),(6,0,1,0.00,0.00,'2017-06-21 12:47:29',2,1),(6,0,1,45.50,0.00,'2017-06-21 12:47:29',3,1),(6,0,1,425.20,0.00,'2017-06-21 12:47:29',4,1),(12,619,1,209969.00,0.00,'2017-06-21 12:47:29',0,1),(13,549,1,172447.00,0.00,'2017-06-21 12:47:29',0,1),(14,508,1,197585.00,0.00,'2017-06-21 12:47:29',0,1),(15,473,2,116456.00,0.00,'2017-06-21 12:47:29',0,1),(15,605,1,41538.00,0.00,'2017-06-21 12:47:29',0,1),(15,613,1,131658.00,0.00,'2017-06-21 12:47:29',0,1),(15,0,1,0.00,0.00,'2017-06-21 12:47:29',2,1),(15,0,4,182.00,0.00,'2017-06-21 12:47:29',3,1),(16,524,5,323615.00,0.00,'2017-06-21 12:47:29',0,1),(16,0,1,0.00,0.00,'2017-06-21 12:47:29',2,1),(17,471,1,67729.00,0.00,'2017-06-21 12:47:29',0,1),(17,0,1,0.00,0.00,'2017-06-21 12:47:29',2,1),(18,473,2,116456.00,0.00,'2017-06-21 12:47:29',0,1),(18,0,2,0.00,0.00,'2017-06-21 12:47:29',2,1),(19,627,1,45000.00,0.00,'2017-06-21 12:47:29',0,1),(20,508,1,197585.00,0.00,'2017-06-21 12:47:29',0,1),(21,622,1,126099.00,0.00,'2017-06-21 12:47:29',0,1),(22,611,1,112239.00,0.00,'2017-06-21 12:47:29',0,1),(24,510,1,55770.00,0.00,'2017-06-21 12:47:29',0,1),(24,0,1,0.00,0.00,'2017-06-21 12:47:29',2,1),(25,605,1,41538.00,0.00,'2017-06-21 12:47:29',0,1),(25,0,1,45.50,0.00,'2017-06-21 12:47:29',3,1),(25,0,1,425.20,0.00,'2017-06-21 12:47:29',4,1),(26,490,1,42000.00,0.00,'2017-06-21 15:52:32',0,2),(26,469,1,43200.00,0.00,'2017-06-21 15:52:32',0,2),(27,501,1,137720.00,0.00,'2017-06-21 15:53:32',0,2),(27,0,5,227.50,0.00,'2017-06-21 15:53:32',3,2),(28,613,1,131658.00,0.00,'2017-06-21 23:37:07',0,1),(28,511,1,54705.00,0.00,'2017-06-21 23:37:07',0,1),(28,601,2,378980.00,0.00,'2017-06-21 23:37:07',0,1),(28,488,1,133836.00,0.00,'2017-06-21 23:37:07',0,1),(28,619,1,209969.00,0.00,'2017-06-21 23:37:07',0,1),(28,614,1,73474.00,0.00,'2017-06-21 23:37:07',0,1),(28,605,1,41538.00,0.00,'2017-06-21 23:37:07',0,1),(28,0,3,0.00,0.00,'2017-06-21 23:37:07',2,1),(29,620,1,54200.00,0.00,'2017-06-22 19:31:34',0,1),(29,474,1,120089.00,0.00,'2017-06-22 19:31:34',0,1),(29,517,1,80391.00,0.00,'2017-06-22 19:31:34',0,1),(29,526,1,185226.00,0.00,'2017-06-22 19:31:34',0,1),(29,547,1,77099.00,0.00,'2017-06-22 19:31:34',0,1),(29,0,1,88.99,0.00,'2017-06-22 19:31:34',1,1),(29,0,7,0.00,0.00,'2017-06-22 19:31:34',2,1),(31,614,1,73474.00,0.00,'2017-06-23 01:34:29',0,1),(31,604,1,35623.00,0.00,'2017-06-23 01:34:29',0,1),(31,0,1,252.98,0.00,'2017-06-23 01:34:29',4,1),(30,511,3,164115.00,0.00,'2017-06-23 01:46:17',0,1),(30,508,1,197585.00,0.00,'2017-06-23 01:46:17',0,1),(30,512,1,227706.00,0.00,'2017-06-23 01:46:17',0,1),(30,0,9,800.91,0.00,'2017-06-23 01:46:17',1,1),(33,510,4,223080.00,0.00,'2017-06-23 02:10:07',0,1),(33,508,1,197585.00,0.00,'2017-06-23 02:10:07',0,1),(33,486,1,170989.00,0.00,'2017-06-23 02:10:07',0,1),(33,511,1,54705.00,0.00,'2017-06-23 02:10:07',0,1),(33,0,1,0.00,0.00,'2017-06-23 02:10:07',2,1),(32,619,1,209969.00,0.00,'2017-06-23 02:11:16',0,1),(32,508,1,197585.00,0.00,'2017-06-23 02:11:16',0,1),(32,510,1,55770.00,0.00,'2017-06-23 02:11:16',0,1),(32,529,1,186336.00,0.00,'2017-06-23 02:11:16',0,1),(32,533,1,209780.00,0.00,'2017-06-23 02:11:16',0,1);
/*!40000 ALTER TABLE `fac_deta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idestado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idmarca`),
  KEY `fk_marca_estado1_idx` (`idestado`),
  CONSTRAINT `fk_marca_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'Mercedes Benz','2017-06-19 08:04:39',1),(2,'BMW','2017-06-19 08:04:39',1),(3,'Porsche','2017-06-19 08:04:39',1),(4,'Kia','2017-06-19 08:04:39',1),(5,'Honda','2017-06-19 08:04:39',1),(6,'Toyota','2017-06-19 08:04:39',1),(7,'Bugatti','2017-06-19 08:04:39',1),(8,'Volkswagen','2017-06-19 08:04:39',1),(9,'Ford','2017-06-19 08:04:39',1),(10,'Maserati','2017-06-19 08:04:39',1),(11,'Nissan','2017-06-19 08:04:39',1),(12,'Chevrolet','2017-06-19 08:04:39',1),(13,'Volvo','2017-06-19 08:04:39',1),(14,'Mazda','2017-06-19 08:04:39',1),(15,'Mitsubishi','2017-06-19 08:04:39',1),(16,'Renault','2017-06-19 08:04:39',1),(17,'Peugeot','2017-06-19 08:04:39',1),(18,'Hyundai','2017-06-19 08:04:39',1),(19,'Lamborghini','2017-06-19 08:04:39',1),(20,'Ferrari','2017-06-19 08:04:39',1),(21,'Audi','2017-06-19 08:04:39',1);
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `apaterno` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `amaterno` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `dni` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `fecha_nacimiento` date NOT NULL,
  `email` varchar(145) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `domicilio` varchar(145) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `idestado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idpersona`),
  UNIQUE KEY `dni_UNIQUE` (`dni`),
  KEY `fk_persona_estado1_idx` (`idestado`),
  CONSTRAINT `fk_persona_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Manuel Ricardo','Palma','Carrillo','09974587','01-7080000','1983-02-07','rpalma@gmail.com','Av. Alfredo Benavides 5440, Santiago de Surco 15039',1),(2,'Joel','Rodriguez','Aparicio','04127485','941784523','1985-04-05','jrodriapa@gmail.com','Jr. Las Magnolias 462',1),(3,'Rodrigo','Bethancourt','Baltazar','00741245','974154245','1980-03-04','rbethancourt@dell.com.pe','Av. la Begonias 724',1),(4,'Diana','Mejia','Rosas','08741542','983 701 425â€‹','1990-02-02','dmejia@gmail.com','Av. Colonial 625',1),(5,'Dayana','Rojas','Arbieto','54174856','974 512 587','1994-06-04','drojas@hotmail.com','Av. Proceres de la Independencia 875',1),(6,'Rolando','Young','Abelardo','00874152','941214745','1987-09-22','ryoungabel@hotmail.es','Av. San Felipe 457',1),(7,'Juan Carlo','Arcila','Dias','08231542','974158452','1989-08-18','jarcila@dreams.pe','Av. Felipe Pardo y Aliaga 1245',1),(8,'Diego','BolaÃ±os','Gonzales','74154251','945147852','1974-09-08','die@hotmail.com','Av. Casa Blanca 456',1),(9,'Alex','Cruces','Rojas','41274578','941214756','1993-04-06','acruces@gmail.com','Av. Aequipa 425',1),(10,'Michael','Alcocer','Puermape','412745896','451247458','1970-04-02','','Av. Faucet 1245',1);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_comprobante`
--

DROP TABLE IF EXISTS `tipo_comprobante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_comprobante` (
  `idtipo_c` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idtipo_c`),
  KEY `fk_tipo_comprobante_idestadi1_idx` (`idestado`),
  CONSTRAINT `fk_tipo_comprobante_idestadi1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_comprobante`
--

LOCK TABLES `tipo_comprobante` WRITE;
/*!40000 ALTER TABLE `tipo_comprobante` DISABLE KEYS */;
INSERT INTO `tipo_comprobante` VALUES (1,'Boleta de Venta',1,'2017-06-21 08:41:41'),(2,'Factura',1,'2017-06-21 08:41:41'),(3,'Letra de Pago',1,'2017-06-21 08:41:41'),(4,'Orden de Compra',1,'2017-06-21 08:41:41');
/*!40000 ALTER TABLE `tipo_comprobante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `passwordbk` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idpersona` int(11) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_persona_idx` (`idpersona`),
  KEY `fk_usuario_estado1_idx` (`idestado`),
  CONSTRAINT `fk_usuario_estado1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_persona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','09974587rp','2017-06-22 20:17:34',1,3),(2,'dmejia','c6bfb0419ca2af3ce5862ca5a0b7129d','08741542MR','2017-06-20 00:41:59',4,3),(3,'drojasa','9ffb078689ef27b5ef5628477a5cb8ff','54174856RA','2017-06-20 00:53:52',5,3),(4,'ryoung','d8c20ba265e62db8ad6c64017c41c2f7','00874152YA','2017-06-20 00:58:47',6,3);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-22 22:38:55
