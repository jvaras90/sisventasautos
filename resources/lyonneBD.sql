-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema lyonneBD
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema lyonneBD
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lyonneBD` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `lyonneBD` ;

-- -----------------------------------------------------
-- Table `lyonneBD`.`estado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`estado` (
  `idestado` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(145) NOT NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  PRIMARY KEY (`idestado`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`persona` (
  `idpersona` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombres` VARCHAR(145) NOT NULL COMMENT '',
  `apaterno` VARCHAR(145) NOT NULL COMMENT '',
  `amaterno` VARCHAR(145) NOT NULL COMMENT '',
  `dni` VARCHAR(15) NOT NULL COMMENT '',
  `telefono` VARCHAR(15) NOT NULL COMMENT '',
  `fecha_nacimiento` DATE NOT NULL COMMENT '',
  `email` VARCHAR(145) NOT NULL COMMENT '',
  `domicilio` VARCHAR(145) NOT NULL COMMENT '',
  `idestado` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idpersona`)  COMMENT '',
  UNIQUE INDEX `dni_UNIQUE` (`dni` ASC)  COMMENT '',
  INDEX `fk_persona_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_persona_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`usuario` (
  `idusuario` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `usuario` VARCHAR(145) NOT NULL COMMENT '',
  `password` VARCHAR(145) NOT NULL COMMENT '',
  `passwordbk` VARCHAR(145) NOT NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  `idpersona` INT NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 3 COMMENT '',
  PRIMARY KEY (`idusuario`)  COMMENT '',
  INDEX `fk_usuario_persona_idx` (`idpersona` ASC)  COMMENT '',
  INDEX `fk_usuario_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_usuario_persona`
    FOREIGN KEY (`idpersona`)
    REFERENCES `lyonneBD`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`cargo` (
  `idcargo` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(145) NOT NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`idcargo`)  COMMENT '',
  INDEX `fk_cargo_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_cargo_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`colaborador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`colaborador` (
  `idcolaborador` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idpersona` INT NOT NULL COMMENT '',
  `fecha_ingreso` DATE NOT NULL COMMENT '',
  `fecha_desceso` DATE NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  `idcargo` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idcolaborador`)  COMMENT '',
  INDEX `fk_colaborador_persona1_idx` (`idpersona` ASC)  COMMENT '',
  INDEX `fk_colaborador_cargo1_idx` (`idcargo` ASC)  COMMENT '',
  CONSTRAINT `fk_colaborador_persona1`
    FOREIGN KEY (`idpersona`)
    REFERENCES `lyonneBD`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_colaborador_cargo1`
    FOREIGN KEY (`idcargo`)
    REFERENCES `lyonneBD`.`cargo` (`idcargo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`marca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`marca` (
  `idmarca` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NOT NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`idmarca`)  COMMENT '',
  INDEX `fk_marca_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_marca_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`color`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`color` (
  `idcolor` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(145) NOT NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`idcolor`)  COMMENT '',
  INDEX `fk_color_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_color_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`autos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`autos` (
  `idautos` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `modelo` VARCHAR(45) NOT NULL COMMENT '',
  `precio` DOUBLE(10,2) NOT NULL COMMENT '',
  `placa` VARCHAR(45) NOT NULL COMMENT '',
  `descripcion` VARCHAR(255) NOT NULL COMMENT '',
  `anio` VARCHAR(145) NOT NULL COMMENT '',
  `idmarca` INT NOT NULL COMMENT '',
  `idcolor` INT NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 1 COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  PRIMARY KEY (`idautos`)  COMMENT '',
  UNIQUE INDEX `placa_UNIQUE` (`placa` ASC)  COMMENT '',
  INDEX `fk_autos_marca1_idx` (`idmarca` ASC)  COMMENT '',
  INDEX `fk_autos_color1_idx` (`idcolor` ASC)  COMMENT '',
  INDEX `fk_autos_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_autos_marca1`
    FOREIGN KEY (`idmarca`)
    REFERENCES `lyonneBD`.`marca` (`idmarca`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_autos_color1`
    FOREIGN KEY (`idcolor`)
    REFERENCES `lyonneBD`.`color` (`idcolor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_autos_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`cliente` (
  `idcliente` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `RUC` VARCHAR(45) NULL COMMENT '',
  `idpersona` INT NOT NULL COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  PRIMARY KEY (`idcliente`)  COMMENT '',
  INDEX `fk_cliente_persona1_idx` (`idpersona` ASC)  COMMENT '',
  CONSTRAINT `fk_cliente_persona1`
    FOREIGN KEY (`idpersona`)
    REFERENCES `lyonneBD`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`fac_cabe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`fac_cabe` (
  `idfac_cabe` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `fecha_emision` DATETIME NOT NULL COMMENT '',
  `idcliente` INT NOT NULL COMMENT '',
  `total` DOUBLE(10,2) NOT NULL COMMENT '',
  `idcolaborador` INT NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 6 COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  PRIMARY KEY (`idfac_cabe`)  COMMENT '',
  INDEX `fk_fac_cabe_cliente1_idx` (`idcliente` ASC)  COMMENT '',
  INDEX `fk_fac_cabe_colaborador1_idx` (`idcolaborador` ASC)  COMMENT '',
  INDEX `fk_fac_cabe_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_fac_cabe_cliente1`
    FOREIGN KEY (`idcliente`)
    REFERENCES `lyonneBD`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_colaborador1`
    FOREIGN KEY (`idcolaborador`)
    REFERENCES `lyonneBD`.`colaborador` (`idcolaborador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`bono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`bono` (
  `idbono` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(145) NOT NULL COMMENT '',
  `precio` DOUBLE(10,2) NOT NULL COMMENT '',
  `idestado` INT NOT NULL DEFAULT 1 COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  PRIMARY KEY (`idbono`)  COMMENT '',
  INDEX `fk_bono_estado1_idx` (`idestado` ASC)  COMMENT '',
  CONSTRAINT `fk_bono_estado1`
    FOREIGN KEY (`idestado`)
    REFERENCES `lyonneBD`.`estado` (`idestado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lyonneBD`.`fac_deta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lyonneBD`.`fac_deta` (
  `idfac_cabe` INT NOT NULL COMMENT '',
  `idautos` INT NOT NULL COMMENT '',
  `cantidad` INT NOT NULL COMMENT '',
  `importe` DOUBLE(10,2) NOT NULL COMMENT '',
  `descuento` DOUBLE(10,2) NOT NULL DEFAULT 0.00 COMMENT '',
  `idbono` INT NOT NULL DEFAULT 1 COMMENT '',
  `lastupdated` TIMESTAMP NOT NULL COMMENT '',
  INDEX `fk_fac_cabe_has_autos_autos1_idx` (`idautos` ASC)  COMMENT '',
  INDEX `fk_fac_cabe_has_autos_fac_cabe1_idx` (`idfac_cabe` ASC)  COMMENT '',
  INDEX `fk_fac_deta_bono1_idx` (`idbono` ASC)  COMMENT '',
  CONSTRAINT `fk_fac_cabe_has_autos_fac_cabe1`
    FOREIGN KEY (`idfac_cabe`)
    REFERENCES `lyonneBD`.`fac_cabe` (`idfac_cabe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_cabe_has_autos_autos1`
    FOREIGN KEY (`idautos`)
    REFERENCES `lyonneBD`.`autos` (`idautos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fac_deta_bono1`
    FOREIGN KEY (`idbono`)
    REFERENCES `lyonneBD`.`bono` (`idbono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Habilitado');
INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Deshabilitado');
INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Offline');
INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Online');
INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Pendiente de pago');
INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Pagado');
INSERT INTO `lyonneBD`.`estado` (`descripcion`) VALUES ('Eliminado');
INSERT INTO `lyonneBD`.`bono` (`descripcion`, `precio`) VALUES ('Ninguno', '0');
INSERT INTO `lyonneBD`.`cargo` (`descripcion`) VALUES ('Gerente');
INSERT INTO `lyonneBD`.`cargo` (`descripcion`) VALUES ('Administrador');
INSERT INTO `lyonneBD`.`cargo` (`descripcion`) VALUES ('Vendedor');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Rojo');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Blanco');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Negro');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Azul');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Verde Oscuro');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Plomo');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Acero');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Crema');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Arena');
INSERT INTO `lyonneBD`.`color` (`descripcion`) VALUES ('Anaranjado');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Mercedes Benz');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('BMW');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Porsche');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Kia');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Honda');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Toyota');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Bugatti');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Volkswagen');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Ford');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Maserati');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Nissan');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Chevrolet');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Volvo');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Mazda');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Mitsubishi');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Renault');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Peugeot');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Hyundai');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Lamborghini');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Ferrari');
INSERT INTO `lyonneBD`.`marca` (`descripcion`) VALUES ('Audi');

INSERT INTO `lyonneBD`.`persona` (`nombres`, `apaterno`, `amaterno`, `dni`, `telefono`, `fecha_nacimiento`, `email`, `domicilio`) VALUES ('Manuel Ricardo', 'Palma', 'Carrillo', '09974587', '01-7080000', '1883-02-07', 'rpalma@gmail.com', 'Av. Alfredo Benavides 5440, Santiago de Surco 15039');

INSERT INTO `lyonneBD`.`usuario` (`usuario`, `password`, `passwordbk`, `idpersona`) VALUES ('admin', md5('admin'), '09974587rp', '1');



 /*

Cargo:
Gerente
Administrador
Vendedor

Color:
blanco
negro
rojo
azul
*/
