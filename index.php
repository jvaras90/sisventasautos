<?php
session_start();
if ($_SESSION['idpersonal'] != "") {
    header("Location: view/index_2.php");
}
$tittle_login = "Acceso | Lyonne Autos";
$fondo = "white-smoke";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo "$tittle_login"; ?></title>
        <link href="images/logo.ico" rel="shortcut icon" />
        <link rel="stylesheet" type="text/css" href="styles/login.css" />
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/functions.ajax.js"></script> 
        <script>
            function txt2Focus() {
                document.getElementById("login_userpass").focus();
            }

            function botonClick() {
                document.getElementById("login_userbttn").focus();
            }
            function validateEnter(e) {
                var key = e.keyCode || e.which;
                if (key == 13) {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div id="allContent">
            <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"  style="background:<?php echo $fondo ?>;">
                <tr>
                    <td align="center" valign="middle" height="100%" width="100%">
                        <div >                            
                            <p><img style="height: 200px; width: 450px;" src="styles/images/logo.jpg"></p>
                        </div>
                        <span class="loginBlock">
                            <span class="inner"  style="background:<?php echo $fondo ?>;">
                                <?php
                                $ipvisitante = $_SERVER["REMOTE_ADDR"];
                                if (isset($_SESSION['username']) && isset($_SESSION['idpersona'])) {
                                    header("Location: view/index_2.php");
                                    die();
                                } else {
                                    ?>
                                    <form method="post" action="">
                                        <table cellpadding="0" cellspacing="0" border="0"  style="background:<?php echo $fondo ?>;">
                                            <tr>
                                                <td>Usuario:</td>
                                                <td>
                                                    <input type="text" name="login_username" id="login_username" onkeyup="if (validateEnter(event) == true) {
                                                                txt2Focus();
                                                            }"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Contrase&ntilde;a:</td>
                                                <td>
                                                    <input type="password" name="login_userpass" id="login_userpass" onkeyup="if (validateEnter(event) == true) {
                                                                botonClick();
                                                            }"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr> 
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <div id="alertBoxes"></div>
                                                    <span class="timer" id="timer"></span> 
                                                    <input 
                                                        id="login_userbttn" type="button" value="Login" name="nuevaCampana" class="boton azul" onclick="this.disabled = true;
                                                                this.value = 'Logeando...';//this.form.submit()"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                <?php } ?>
                            </span>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>